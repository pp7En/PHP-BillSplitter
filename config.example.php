<?php

define('ROOT', 'example.com');
define('PUBLIC_DIR', 'public');
define('NOTIFICATIONS_SENDER', 'notification@example.com');
define('NOTIFICATIONS', FALSE);
define('DEBUG_MODE', FALSE);
define('ENABLE_REGISTRATION', TRUE);
define('LANGUAGE', 'en');
//define('LANGUAGE', 'de');

define('ROUTES', 'routes.json');
define('CONTROLLER_DIR', 'controllers');

define('TEMPLATE_DIR', 'view');
define('CSS_DIR','public/css');
define('JS_DIR','public/js');

// Change it if you would like to use a mysql server instead sqlite
define('USE_SQLITE', FALSE);
define('DB_FILE', 'splitter.db');

define('DB_HOST', 'localhost');
define('DB_NAME', 'billsplitter');
define('DB_USER', 'billsplitter');
define('DB_PASSWORD','Use_STRONG_PassWORD_for_BillSplitter');
