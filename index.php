<?php

    date_default_timezone_set('Europe/Berlin');

    ob_start('ob_gzhandler');

    set_error_handler(function ($severity, $message, $file, $line) {
            throw new ErrorException($message, 1, $severity, $file, $line);
    });

    spl_autoload_register(function ($class) {
        $file = __DIR__ . '/src/' . str_replace('\\', '/', $class) . '.php';
        if (file_exists($file)) {
            require $file;
        }
    });

    /// PHP compat

    if (!function_exists('http_response_code')) {
        function http_response_code($code) {
            header("{$_SERVER['SERVER_PROTOCOL']} {$code}", true, $code);
        }
    }


    require 'config.php';

    // #########################################################################
    // Set CSP
    $CspString = "";
    $CspString .= "default-src 'none';";
    $CspString .= "block-all-mixed-content;";
    $CspString .= "connect-src 'self';";
    $CspString .= "style-src 'self' ;";
    $CspString .= "form-action 'self';";
    $CspString .= "script-src 'self';";
    $CspString .= "img-src 'self' data: ;";
    $CspString .= "font-src 'self' ;";
    $CspString .= "frame-ancestors 'self';";
    $CspString .= "base-uri 'self';";
    $CspString .= "object-src 'none';";
    $CspString .= "frame-src 'self';";
    $CspString .= "child-src 'self';";
    $CspString .= "manifest-src 'self';";
    $CspString .= "media-src 'self';";
    $CspString .= "worker-src 'self';";
    header("Content-Security-Policy: " . $CspString);

    // #########################################################################
    // Set HSTS
    header("Strict-Transport-Security: max-age=63072000; includeSubDomains; preload");

    // #########################################################################
    // Set PermissionPolicy
    $PermissionPolicy = "";
    $PermissionPolicy .= "accelerometer=(),";
    $PermissionPolicy .= "ambient-light-sensor=(),";
    $PermissionPolicy .= "autoplay=(),";
    $PermissionPolicy .= "battery=(),";
    $PermissionPolicy .= "camera=(),";
    $PermissionPolicy .= "cross-origin-isolated=(),";
    $PermissionPolicy .= "display-capture=(),";
    $PermissionPolicy .= "document-domain=(),";
    $PermissionPolicy .= "encrypted-media=(),";
    $PermissionPolicy .= "execution-while-not-rendered=(),";
    $PermissionPolicy .= "execution-while-out-of-viewport=(),";
    $PermissionPolicy .= "fullscreen=(),";
    $PermissionPolicy .= "geolocation=(),";
    $PermissionPolicy .= "gyroscope=(),";
    $PermissionPolicy .= "keyboard-map=(),";
    $PermissionPolicy .= "magnetometer=(),";
    $PermissionPolicy .= "microphone=(),";
    $PermissionPolicy .= "midi=(),";
    $PermissionPolicy .= "navigation-override=(),";
    $PermissionPolicy .= "payment=(),";
    $PermissionPolicy .= "picture-in-picture=(),";
    $PermissionPolicy .= "publickey-credentials-get=(),";
    $PermissionPolicy .= "screen-wake-lock=(),";
    $PermissionPolicy .= "sync-xhr=(),";
    $PermissionPolicy .= "usb=(),";
    $PermissionPolicy .= "web-share=(),";
    $PermissionPolicy .= "xr-spatial-tracking=(),";
    $PermissionPolicy .= "clipboard-read=(),";
    $PermissionPolicy .= "clipboard-write=(),";
    $PermissionPolicy .= "gamepad=(),";
    $PermissionPolicy .= "speaker-selection=(),";
    $PermissionPolicy .= "conversion-measurement=(),";
    $PermissionPolicy .= "focus-without-user-activation=(),";
    $PermissionPolicy .= "hid=(),";
    $PermissionPolicy .= "idle-detection=(),";
    $PermissionPolicy .= "interest-cohort=(),";
    $PermissionPolicy .= "serial=(),";
    $PermissionPolicy .= "sync-script=(),";
    $PermissionPolicy .= "trust-token-redemption=(),";
    $PermissionPolicy .= "window-placement=(),";
    $PermissionPolicy .= "vertical-scroll=()";
    header("Permissions-Policy: " . $PermissionPolicy);

    header("Referrer-Policy: no-referrer");
    header("X-Content-Type-Options: nosniff");

    if (LANGUAGE == "de") {
    	include './public/i18n/lang_de.php';
    } else {
    	include './public/i18n/lang_en.php';
    }


    function routeRequest($route) {
        if (strlen($route) > 0 && $route[0] != '/') {
            $route = "/{$route}";
        }
        $file = PUBLIC_DIR . $route;
        // Public file response (in case .htaccess is not used)
        if (is_file($file)) {
            $mTime = filemtime($file);
            $etag = md5_file($file);
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s', $mTime) . ' GMT');
            header("Etag: {$etag}");

            // css seems to be detected as text/plain
            if(strpos($route, '/css/') === 0) {
                $type = 'text/css';
                header("Cache-Control: public, max-age=31536000, immutable");
            } else if(strpos($route, '/js/') === 0 || strpos($route, '/i18n/') === 0) {
                $type = 'text/javascript';
                header("Cache-Control: public, max-age=31536000, immutable");
            } else {
                $type = mime_content_type($file);
            }
            header("Content-Type: $type");
            if ((isset($_SERVER['HTTP_IF_NONE_MATCH']) && trim($_SERVER['HTTP_IF_NONE_MATCH']) == $etag)
                || (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) == $mTime)) {
                http_response_code(304);
            } else {
                readfile(PUBLIC_DIR . $route);
            }
            return;
        }
        $routes = json_decode(file_get_contents(ROUTES));

        if ($routes === null) {
            ErrorHandler::exitNow(500, "Failed to read routes.json");
        }

        $mostSpecific = '';
        $controller = null;
        foreach ($routes as $path => $pathController) {
            if (stripos($route, $path) === 0 && strlen($path) > strlen($mostSpecific)) {
                $mostSpecific = $path;
                $controller = $pathController;
            }
        }

        if ($controller === null) {
            ErrorHandler::exitNow(404, "Unknown route \"$route\"");
        }

        $controllerPath = CONTROLLER_DIR . "/{$controller}.php";
        if (!file_exists($controllerPath)) {
            ErrorHandler::exitNow(500, "Controller file does not exist");
        }

        require $controllerPath;

        $className = "{$controller}Controller";

        $ctlr = new $className();
        if (!($ctlr instanceof App\Controller)) {
            ErrorHandler::exitNow(500, "Constructed controller is not actually a controller");
        }
        $ctlr->handleRequest($route, $mostSpecific);
    }

    if (ROOT === $_SERVER['SCRIPT_NAME'] . '/') {
        $uri = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : '/';
    } else {
        $uri = isset($_GET['route']) ? $_GET['route'] : '/';
    }
    routeRequest($uri);
