<?php
    // General
    $Currency = '$';
    $UnknownErrorOccurred = 'Unknown error occurred';

    // Target: src\AuthManager
    $IncorrectEmailOrPassword = 'Incorrect email or password';
    $AccountNotYetActivated = 'Account not yet activated';
    $EmailAddressAlreadyExists = 'Email address already exists';
    $ActivationMailSended = 'Activation mail sended! Please check your mails';
    $ResetPasswordMailSended = 'Restore link sended! Please check your mails';
    $MustBeLoggedIn = 'Must be logged in';
    $IncorrectOldPassword = 'Incorrect old password';
    $NotAValidationCode = 'Not a validation code';
    $NotAValidGuestCode = 'Invalid invitation code';
    $AccountActivated = 'Account activated!';
    $AccountResetPasswordSuccess = 'Password successfully reset!';
    $DetermineNewAdmin = 'Before you can delete your account you have to determine a new group admin.';
    $InvalidEmailAddress = 'Invalid email address';
    $AccountDeleted = 'Account deleted';
    $RegistrationDisabled = 'Sorry, this function is disabled.';

    // Target: src\Notifications
    $MailSubjectNotifications = 'Notification from BillSplitter';
    $MailMessage = 'Please active your account with this link:';
    $MailSubjectValidation = 'Please activate your account';
    $Balance = 'Balance';
    $PayTo = 'owes money to';

    $MailMessage = 'Apply a new password for your account with this link:';
    $MailSubjectValidation = 'Reset your Password';

    $GroupRequestRejected1 = 'Your membership request for the group "';
    $GroupRequestRejected2 = '" was rejected';

    // Target: src\BillModel
    $NewBillAvailable = 'A new bill has been added.'."\r\n\r\n".'Your share is';
    $PercentagesDontAddUpTo100 = 'Percentages don\'t add up to 100%';
    $UserDoesntBelongToThisGroup = 'User doesn\'t belong to this group';
    $UserIdDoesNotExistOrIsNotPartOfTheGroup = 'User ID does not exist or is not part of the group';
    $InvalidBillId = 'Invalid Bill ID';
    $UserIdArrayDoesntMatchPropotionen = 'User ID array to split with does not match the length of the proportions';
    $ProportionPercentageInArrayIsNotValidInteger = 'Proportion percentage in array is not a valid integer';
    $ProportionMustBeBetween1and100 = 'Proportion must be between 1 and 100';
    $UserIdInArrayIsNotValidInteger = 'User ID in array is not a valid integer';
    $FailedToCreateBill = 'Failed to create bill';
    $FailedToDeletePayment = 'Failed to delete payment';

    // Target: src\GroupModel
    $UserAlreadyHasMembershipGroup = 'User already has a membership with this group';
    $UnknownUserID = 'Unknown user ID';
    $NoUserWithMailAddress = 'No user exists with that email address';
    $ThatsYourAddress = 'That\'s your email!';
    $ThatUserIsNotRegisteredToGroup = 'You are not yet registered in any group';
    $YouAreNotInGroup = 'You\'re not in a group';
    $WaitingForConfirmation = 'Group request sent. Waiting for confirmation';
    $AccountUnbalanced = 'Account unbalanced. Please balance it first.';
    $GuestUserNameCannotBeEmpty = 'The user name cannot be empty!';
    $YouAreNotAdminOfYourGroup = 'You are not admin of your group!';
    $CannotAddGuestUserToUsersTable = 'Error! Cannot add guest user to table \'users\' ';
    $CannotAddGuestUserToMappingTable = 'Error! Cannot add guest user to table \'mapping\' ';
    $FailedToCreateGroup = 'Failed to create group';
    $FailedToRequestJoiningThatGroup = 'Failed to request joining that group';
    $DifferentGroups = 'Different groups';

?>
