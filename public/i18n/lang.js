$(document).ready(function() {
    // Main menu
    $('#menu-title-groups').text(lang.Group);
    $('#menu-title-settings').text(lang.Settings);
    $('#menu-group-join-group-content').text(lang.JoinGroup);
    $('#menu-create-popup-content').text(lang.CreateGroup);
    $('#menu-groups').text(lang.Overview);
    $('#menu-payment-history').text(lang.History);
    $('#menu-statistics').text(lang.Statistics);
    $('#menu-settings').text(lang.Settings);
    $('#menu-logout').text(lang.Logout);
    $('#menu-login').text(lang.Login);
    $('#menu-signup').text(lang.SignUp);
    $('#groups-statistic-button').text(lang.Statistic);

    // Activated account
    $('#activation-title').text(lang.UserAccountActivated);
    $('#activation-login').text(lang.LoginNow);

    // Signup
    $('#control-label-register-name').text(lang.Name);
    $('#control-label-register-password-first').text(lang.Password);
    $('#control-label-register-password-second').text(lang.PasswordRepeat);
    $('#button-signup').prop('value', lang.SignUp);

    // Create new bill
    $('#control-label-bill-create-subject').text(lang.ForWhat);
    $('#bill-create-name').text(lang.Name);
    $('#bill-create-amount').text(lang.Amount);
    $('#bill-create-button-add-bill').prop('value', lang.AddBill);
    $('#calculator-apply').text(lang.Apply);
    $('#calculator-cancel').text(lang.Cancel);
    $('#control-label-amount-input-field').text(lang.AmountOfPartlyPayment);
    $('#amount-input-field-error').text(lang.InvalidInput);

    // Group view
    $('#join-button').text(lang.JoinGroup);
    $('#para-add-new-user').text(lang.DescriptionAddNewUser);
    $('#groups-history-button').text(lang.HistoryButton);
    $('#groups-header-title').text(lang.Groups);
    $('#group-description-groups').text(lang.DescriptionGroups);
    $('#group-description-groups-code').text(lang.Code);
    $('#group-join-group').prop('value', lang.SendJoinRequest);
    $('#group-create-group').text(lang.DescriptionCreateGroup);
    $('#group-button-create-group').prop('value', lang.CreateGroup);
    $('#add-user-info').text(lang.QuestionAddUserToGroup);
    $('#control-label-create-group-name').text(lang.NameOfGroup);
    $('#control-label-create-new-user').text(lang.Username);
    $('#create-new-user-btn').prop('value', lang.Add);
    $('#groups-details').text(lang.ShowAllMembers);
    $('#groups-leave-group').text(lang.LeaveGroup);
    $('#groups-create-user').text(lang.CreateNewUser);
    $('#groups-open-settings').text(lang.Options);
    $('#groups-settlement-table').text(lang.SettlementTable);
    $('#groups-create-bill').text(lang.NewBill);
    $('#create-new-bill').text(lang.NewBill);
    $('#full-payment').text(lang.FullPayment);
    $('#instalment-payment').text(lang.InstalmentPayment);
    $('#groups-change-guest-login-button').text(lang.ShareGroup);
    $('#change-guest-login').attr('value', lang.Apply);
    $('#spn-change-guest-login').text(lang.ChangeGuestLogin);

    // Settings view
    $('#settings-notification').text(lang.Notifications);
    $('#spn-notification').text(` ${lang.Enable}`);
    $('#para-notification').text(lang.DescriptionNotifications);
    $('#settings-notification-button').attr('value', lang.Apply);
    $('#settings-profile').text(lang.SettingsProfile);
    $('#control-label-settings-new-name').text(lang.Name);
    $('#control-label-settings-new-email').text(lang.EMail);
    $('#settings-update-button').attr('value', lang.Apply);
    $('#settings-password').text(lang.ChangePassword);
    $('#control-label-settings-new-oldpass').text(lang.OldPassword);
    $('#control-label-settings-new-password-first').text(lang.NewPassword);
    $('#control-label-settings-new-password-second').text(lang.NewPasswordRepeat);
    $('#settings-password-change-button').prop('value', lang.Apply);
    $('#settings-delete').text(lang.DeleteAccount);
    $('#spn-yes-i-am-sure').text(` ${lang.YesIamSure}`);
    $('#control-label-delete-account-password').text(lang.Password);
    $('#settings-delete-account-button').attr('value', lang.DeleteAccount);
    $('#para-registration').text(lang.PublicRegistrations);
    $('#settings-registration').text(lang.Registrations);
    $('#spn-registration').text(` ${lang.Enable}`);
    $('#settings-registration-button').attr('value', lang.Apply);

    // History view
    $('#delete-payment-yes').attr('value', lang.Delete);
    $('#delete-payment-no').attr('value', lang.Cancel);
    $('#amount-input-field-error').text(lang.InputValidationError);
    $('#control-label-search-payments').text(lang.Search);
    $('#content-loading-area').text(lang.Loading);

    // Auth views
    $('#control-label-email-address').text(lang.EMailAddress);
    $('#control-label-password').text(lang.Password);
    $('#para-password-forgotten').text(lang.PasswordForgotten);
    $('#back-to-login').text(lang.BackToLogin);
    $('#control-label-restore-password').text(lang.EMailAddress);
    $('#restore-password-button').prop('value', lang.ResetPassword);
    $('#control-label-apply-new-password-first').text(lang.Password);
    $('#control-label-apply-new-password-second').text(lang.PasswordRepeat);
    $('#apply-new-password-button').prop('value', lang.Send);

    // Settings view
    $('#settings-back-to-main').text(lang.Back);
});
