<?php
    // General
    $Currency = '€';
    $UnknownErrorOccurred = 'Unbekannter Fehler aufgetreten';

    // Target: src\AuthManager
    $IncorrectEmailOrPassword = 'Falsche E-Mail Adresse oder Passwort';
    $AccountNotYetActivated = 'Benutzerkonto noch nicht aktivert';
    $EmailAddressAlreadyExists = 'E-Mail Adresse existiert bereits';
    $ActivationMailSended = 'Aktivierungslink versendet! Bitte E-Mails prüfen';
    $ResetPasswordMailSended = 'Wiederherstellungslink versendet! Bitte E-Mails prüfen';
    $MustBeLoggedIn = 'Anmeldung notwendig';
    $IncorrectOldPassword = 'Falsches (altes) Passwort';
    $NotAValidationCode = 'Kein gültiger Aktivierungscode';
    $NotAValidGuestCode = 'Ungültiger Einladungscode';
    $AccountActivated = 'Benutzerkonto aktiviert!';
    $AccountResetPasswordSuccess = 'Passwort erfolgreich zurückgesetzt!';
    $DetermineNewAdmin = 'Bevor du dein Konto löschen kannst, musst du einen neuen Gruppen-Administrator bestimmen.';
    $InvalidEmailAddress = 'Ungültige E-Mail Adresse';
    $AccountDeleted = 'Account gelöscht';
    $RegistrationDisabled = 'Sorry, diese Funktion wurde deaktiviert.';
    $DataChanged = 'Erfolgreich';

    // Target: src\Notifications
    $MailSubjectNotifications = 'Benachrichtigung von BillSplitter';
    $MailMessageActivateAccount = 'Bitte aktiviere dein Konto mit diesem Link:';
    $MailSubjectActivateAccount = 'Bitte aktiviere dein Benutzerkonto';
    $Balance = 'Kontostand';
    $PayTo = 'schuldet';

    $MailMessageResetPassword = 'Mit diesem Link kannst du ein neues Passwort setzen:';
    $MailSubjectResetPassword = 'Passwort wiederherstellen';

    $GroupRequestRejected1 = 'Deine Beitrittsanfrage für die Gruppe "';
    $GroupRequestRejected2 = '" wurde abgelehnt';

    // Target: src\BillModel
    $NewBillAvailable = 'Neue Ausgabe vorhanden.'."\r\n\r\n".'Dein Anteil beträgt';
    $PercentagesDontAddUpTo100 = 'Die Prozentsätze ergeben nicht 100%';
    $UserDoesntBelongToThisGroup = 'Benutzer gehört nicht zu dieser Gruppe';
    $UserIdDoesNotExistOrIsNotPartOfTheGroup = 'Die Benutzer-ID existiert nicht oder ist nicht Teil der Gruppe';
    $InvalidBillId = 'Ungültige Rechnung ID';
    $UserIdArrayDoesntMatchPropotionen = 'Benutzer-ID-Array, durch das geteilt werden soll, stimmt nicht mit der Länge der Anteile überein';
    $ProportionPercentageInArrayIsNotValidInteger = 'Der prozentuale Anteil im Array ist keine gültige ganze Zahl';
    $ProportionMustBeBetween1and100 = 'Der Anteil muss zwischen 1 und 100 liegen';
    $UserIdInArrayIsNotValidInteger = 'Benutzer-ID in Array ist keine gültige Ganzzahl';
    $FailedToCreateBill = 'Rechnung konnte nicht erstellt werden';
    $FailedToDeletePayment = 'Failed to delete payment';

    // Target: src\GroupModel
    $UserAlreadyHasMembershipGroup = 'Benutzer hat bereits eine Mitgliedschaft in dieser Gruppe';
    $UnknownUserID = 'Unbekannte Benutzer-ID';
    $NoUserWithMailAddress = 'Es existiert kein Benutzer mit dieser E-Mail-Adresse';
    $ThatsYourAddress = 'Das ist deine E-Mail Adresse!';
    $ThatUserIsNotRegisteredToGroup = 'Du bist in noch keiner Gruppe registriert';
    $YouAreNotInGroup = 'Du bist in keiner Gruppe';
    $WaitingForConfirmation = 'Gruppenanfragen gesendet. Warte auf Bestätigung';
    $AccountUnbalanced = 'Konto unausgeglichen. Bitte zuerst Konten ausgleichen.';
    $GuestUserNameCannotBeEmpty = 'Der Benutzername darf nicht leer sein!';
    $YouAreNotAdminOfYourGroup = 'Sie sind nicht der Administrator Ihrer Gruppe!';
    $CannotAddGuestUserToUsersTable = 'Fehler! Gastbenutzer kann nicht zur Tabelle \'users\' hinzugefügt werden';
    $CannotAddGuestUserToMappingTable = 'Fehler! Gastbenutzer kann nicht zur Tabelle \'users\' hinzugefügt werden';
    $FailedToCreateGroup = 'Gruppe konnte nicht erstellt werden';
    $FailedToRequestJoiningThatGroup = 'Beitrittsanfrage zu dieser Gruppe fehlgeschlagen';
    $DifferentGroups = 'Unterschiedliche Gruppen';

?>
