$(document).ready(function () {
    $('#menu-title-settings').addClass('elem-border-top-none');
    let autologin = window.location.hash.substring(1);
    if (autologin.length !== 0) {
        $('#loginpage-login').empty();
        $('#loginpage-login').html(lang.AutoLoggingStarted);
    }
    $('#loading').addClass('elem-hidden').removeClass('elem-show');
});
