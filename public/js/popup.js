(function (document) {
    'use strict';

    let popupOverlay = $('<div id="popup-overlay"></div>'),

        openPopup = null;
    function closeIfOpen() {
        openPopup && openPopup.hide();
    }
    popupOverlay.click(closeIfOpen);

    document.addEventListener('keydown', function (event) {
        event.key === 'Escape' && closeIfOpen();
    });

    $(document).ready(function () {
        $(document.body).append(popupOverlay);
    });

    $.fn.popup = function (options) {
        if (!this.length) {
            return null;
        }
        let elem = this[0],
            popup = $('<div class="popup"></div>');
        popup.click(function (event) {
            event.stopPropagation();
        });
        popup.append(elem);
        popupOverlay.append(popup);
        let origTop = popup.css('top');
        return {
            'show' () {
                if (openPopup !== null) {
                    throw lang.CannotOpenMoreThanOnePopupAtATime;
                }
                popupOverlay.show();
                popup.show();
                popup.css({
                    'top': origTop
                });
                openPopup = this;
                if (options.open) {
                    options.open.call(elem);
                }
            },
            'hide' () {
                popup.css('top', '0');
                popupOverlay.hide();
                popup.hide();
                openPopup = null;
                if (options.close) {
                    options.close.call(elem);
                }
            },
            'element': this
        };
    };

})(document);
