function adminStuff(admin, yourId, member) {
    let numberOfRegisteredUser = 0;
    if(admin) {
        $('#new-admin-listbox').find('option').remove();
        if (yourId !== member.id) {
            $('#new-admin-listbox').append($('<option>').text(member.name).val(member.id));
            if (member.registered === 'yes') {
                numberOfRegisteredUser++;
            }
        }
        if (numberOfRegisteredUser === 0) {
            $('#para-change-admin').html(`${lang.NoOtherRegisteredUser}<br>`);
            $('#para-change-admin').show();
            $('#new-admin-listbox').remove();
        } else {
            $('#para-change-admin').show();
            $('#para-change-admin').html(lang.DetermineNewAdmin);
            $('#new-admin-listbox').show();
        }
    } else {
        $('#new-admin-listbox').remove();
    }
}

function leaveGroup(details, groupId, admin) {
    let unbalanced = false;
    $('#para-leave-group').html('');
    $('#leave-group-yes').show();
    $('#groupIdLeaveGroup').val(groupId);

    if (details.members.length > 1) {
        for (let member of details.members) {
            if(member.paymentsDue !== 0) {
                unbalanced = true;
            }
            adminStuff(admin, details.yourId, member);
        }
    } else {
        $('#para-change-admin').show();
        $('#para-change-admin').html(`${lang.NoOtherRegisteredUser}<br>`);
        $('#new-admin-listbox').remove();
    }
    $('#leave-group-yes').attr('value', lang.ConfirmLeaveGroup);

    if(unbalanced) {
        $('#leave-group-yes,#new-admin-listbox,#para-change-admin').hide();
        $('#para-leave-group').html(lang.CannotLeaveGroup);
    }
}
