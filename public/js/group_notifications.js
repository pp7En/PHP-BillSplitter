$(document).ready(function() {
    $.getJSON('./notifications/get-notifications')
        .done(function (response) {
            for (let message of response) {
                addRejectedMessage(message.id, message.message, 'error');
            }
        })
        .fail(function (xhr) {
            addMessage(xhr.responseText, 'error');
        });
});
