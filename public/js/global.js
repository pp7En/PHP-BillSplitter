function addMessage(message, type, onDismiss) {
    type = type || 'info';
    let messageArea = $('#message-area'),
        messageDiv = $('<div>', {'class': `${type} message`}),
        content = $('<div>', {'class': 'message-content'}),
        dismissBox = $('<div>', {'class': 'dismiss-box'}),
        dismiss;

    content.text(message);
    messageDiv.append(content);

    dismiss = $('<img>', {
        'src': 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNCIgaGVpZ2h0PSIyNCIgdmlld0JveD0iMCAwIDI0IDI0Ij48cGF0aCBkPSJNMTkgNi40MUwxNy41OSA1IDEyIDEwLjU5IDYuNDEgNSA1IDYuNDEgMTAuNTkgMTIgNSAxNy41OSA2LjQxIDE5IDEyIDEzLjQxIDE3LjU5IDE5IDE5IDE3LjU5IDEzLjQxIDEyeiIvPjxwYXRoIGQ9Ik0wIDBoMjR2MjRIMHoiIGZpbGw9Im5vbmUiLz48L3N2Zz4=',
        'alt': 'Close',
        'href': '#',
        'on': {
            'click' (event) {
                event.preventDefault();
                if (!onDismiss || (onDismiss && onDismiss.call(window))) {
                    messageDiv.remove();
                }
            }
        }
    });
    setTimeout(function() {
        messageDiv.fadeOut('fast');
    }, 5000);
    dismissBox.append(dismiss);
    messageDiv.append(dismissBox);
    messageArea.append(messageDiv);
}

function messageNoGroup() {
    addMessage(lang.YouArentMemberOfGroup, 'error');
}

function separator(number) {
    number = `${number}`;
    if (number.length > 3) {
        let mod = number.length % 3,
            output = (mod > 0 ? (number.substring(0, mod)) : '');
        for (let i = 0; i < Math.floor(number.length / 3); i++) {
            if ((mod === 0) && (i === 0)) {
                output += number.substring(mod + 3 * i, mod + 3 * i + 3);
            } else {
                output += lang.Separator + number.substring(mod + 3 * i, mod + 3 * i + 3);
            }
        }
        return (output);
    } else {
        return number;
    }
}

function addRejectedMessage(id, message, type, onDismiss) {
    type = type || 'info';
    let messageArea = $('#message-area'),
        messageDiv = $('<div>', {'class': `${type} message`}),
        content = $('<div>', {'class': 'message-content'}),
        dismissBox = $('<div>', {'class': 'dismiss-box'}),
        dismiss;

    content.text(message);
    messageDiv.append(content);

    dismiss = $('<img>', {
        'src': './close.svg',
        'alt': 'Close',
        'href': '#',
        'on': {
            'click' (event) {
                event.preventDefault();
                if (!onDismiss || (onDismiss && onDismiss.call(window))) {
                    $.ajax({
                        'url': '../notifications/delete-notification',
                        'type': 'POST',
                        'data': {'id': id},
                        'success'() {
                            messageDiv.remove();
                        },
                        'error'() {
                            console.log('Error');
                        }
                    });
                }
            }
        }
    });

    dismissBox.append(dismiss);
    messageDiv.append(dismissBox);
    messageArea.append(messageDiv);
}

$(document).ready(function() {
    $('#toggle-button').click(function() {
        $('#toggle-menu').toggle();
        $('#toggle-button').toggleClass('change');
    });
    $('#menu-right').click(function() {
        $('#toggle-menu').toggle();
        $('#toggle-button').toggleClass('change');
    });

    $('.content').toggleClass('active', true);
});
