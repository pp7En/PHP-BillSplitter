function createRemoveUserPopup(groupId, member) {
    let removeUserPopup = $('#remove-user-popup').popup({
        'open' () {
            if(member.paymentsDue === 0) {
                $('#para-remove-user').html(`${lang.PopupRemoveUser}<br>${member.name}`);
                $('#remove-user-yes').show();
                $('#remove-user-yes').prop('value', lang.PopupRemoveUserBtn);
            } else {
                $('#para-remove-user').html(lang.CannotRemoveUser);
                $('#remove-user-yes').hide();
            }
            $('#remove-user-no').prop('value', lang.Cancel);
        }
    });
    if(member.paymentsDue === 0) {
        removeUserPopup.element.submit(function (e) {
            e.preventDefault();
            $.post('./group/remove-user', {'userId': member.id, 'groupId': groupId})
                .done(function () {
                    removeUserPopup.hide();
                    addMessage(lang.UserRemoved, 'info');
                    location.reload();
                })
                .fail(function (xhr) {
                    addMessage(xhr.responseText, 'error');
                    removeUserPopup.hide();
                });
        });
    }
    $('#remove-user-no').click(function() {
        removeUserPopup.hide();
    });
    removeUserPopup.show();
    $('#remove-user-popup').show();
}

function iterateMembers(groupId, admin, member, counter) {
    let divHighlight;

    if(counter % 2 === 0) {
        divHighlight = 'member-dark';
    } else {
        divHighlight = 'member-light';
    }

    if(counter === 0) {
        $('#members-table-div').append('<div class="member-frame"></div>');
        $('.member-frame:last').append('<div class="div-member div-member-header"></div>');
        $('.div-member:last').append(`<div class="div-member-name div-member-header-left">${lang.User}</div>`);
        $('.div-member:last').append(`<div class="div-member-payments div-member-header-center">${lang.Paid}</div>`);
        $('.div-member:last').append(`<div class="div-member-balance div-member-header-right">${lang.Balance}</div>`);
    }
    $('#members-table-div').append(`<div class="member-frame ${divHighlight}"></div>`);
    $('.member-frame:last').append('<div class="div-member"></div>');

    if(admin) {
        $('.div-member:last').append(`<div class="${divHighlight} div-member-name"><div class="member-delete-icon">${member.name}</div></div>`);
    } else {
        $('.div-member:last').append(`<div class="${divHighlight} div-member-name">${member.name}</div>`);
    }

    $('.member-delete-icon:last').bind('click', function () {
        createRemoveUserPopup(groupId, member);
    });
    $('.div-member:last').append(`<div class="${divHighlight} div-member-payments">${member.paymentsMade}</div>`);
    $('.div-member:last').append(`<div class="${divHighlight} div-member-balance">${(parseFloat(member.paymentsDue)).toFixed(2).replace('.', ',')}&nbsp;${lang.Currency}</div>`);

    if(parseFloat(member.paymentsDue) < 0) {
        $('.div-member-balance:last').addClass('balanceRed');
    } else {
        $('.div-member-balance:last').addClass('balanceGreen');
    }
}

function createMemberTable(details, admin, groupId) {
    if(details === null) {
        $('#noGroup').show();
        $('#group').hide();
        return;
    }
    $('#noGroup').hide();
    $('#group').show();
    if(!admin) {
        $('#create-new-user').remove();
    }
    $('#members-table-div').empty();
    $('#groupId').val(groupId);
    $('#members-table-div').append(`<h3>${lang.Member}</h3>`);
    for (let [ind, member] of details.members.entries()) {
        iterateMembers(groupId, admin, member, ind);
    }
    $('#create-new-user-btn').prop('disabled', true);
    $('#create-new-user').show();
    $('#guestname').on('keyup input change', function () {
        if($('#guestname').val() === 0) {
            $('#create-new-user-btn').prop('disabled', true);
        } else {
            $('#create-new-user-btn').prop('disabled', false);
        }
    });
}

function createNewUser() {
    let guestname = $('#guestname').val(),
        groupId = $('#groupId').val();
    if(guestname.length < 2) {
        addMessage(lang.UsernameToShort, 'error');
        return;
    }
    $.post('./group/create-new-user', {'guestname': guestname, 'groupId': groupId})
        .done(function () {
            addMessage(lang.ThereWasNoError, 'info');
            getGroups();
        })
        .fail(function (xhr) {
            addMessage(xhr.responseText, 'error');
        });
}
