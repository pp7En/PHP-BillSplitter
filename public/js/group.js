function buildAllGroups(response) {
    $('#menu-top-groups').empty();
    for(let i = 0; i < response.length; i++) {
        let buttonGroup = $(`<a class="menu-item">${response[i].name}</a>`);
        buttonGroup.prependTo('#menu-top-groups');
        buttonGroup.unbind().bind('click', function () {
            toggleGroup(response[i].gid, response[i].name, response[i].code, response[i].suggestions);
        });
    }
}

function toggleMenuPoint() {
    $('#toggle-menu').toggle();
    $('#toggle-button').toggleClass('change');
    $('.content').each(function () {
        $(this).hide();
    });
    $('#groups-settings-div,#groups-create-group').hide();
}

function toggleGroup(groupId, groupName, groupCode, suggestions) {
    let result = suggestions.map((a) => a.text),
        now = new Date(),
        day = (`0${now.getDate()}`).slice(-2),
        month = (`0${now.getMonth() + 1}`).slice(-2),
        today = `${now.getFullYear()}-${month}-${day}`;

    fetchDetails(groupId);

    $('#history-div').empty();
    $('#desc-input-field-datalist').empty();

    $('#groups-header-title').html(groupName);
    $('#group-code').html(`${lang.InvitationCode}: ${groupCode}`);
    $('.group-menu-ind,.content').each(function () {
        $(this).hide();
    });
    $('#groups-settings,#join-or-create-group').hide();
    $('#groups-settings-div').show();

    $('.group-action').each(function () {
        $(this).show();
    });
    $('#groups-create-bill-div').show();

    $('#toggle-menu').toggle();
    $('#toggle-button').toggleClass('change');

    $('#amount-input-field-error').addClass('elem-hidden');
    $('#desc-input-field').val(null);
    $('#amount-input-field').val(null);

    $('#date-input-field').val(today);

    $('#desc-input-field').unbind().bind('change input', function (e) {
        let inputFieldValue = $('#desc-input-field').val(),
            filtered;
        $('#desc-input-field-datalist').empty();

        validateFields(e);

        if(inputFieldValue.length === 0) {
            $('#desc-input-field-datalist').removeClass('desc-input-field-datalist-class');
            return;
        }

        $('#desc-input-field-datalist').addClass('desc-input-field-datalist-class');

        filtered = result.filter((n) => n.toLowerCase().includes(inputFieldValue.toLowerCase()));
        createOwnDatalist(filtered);
    });
}

function createOwnDatalist(suggestions) {
    if(suggestions.length === 0) {
        $('#desc-input-field-datalist').removeClass('desc-input-field-datalist-class');
        return;
    }
    suggestions.forEach(function(str) {
        let pElement = $('<p></p>'),

            spanElement = $('<span></span>').text(str);
        spanElement.addClass('p-suggestion');
        spanElement.unbind().bind('click', function() {
            $('#desc-input-field').val(spanElement.text());
            $('#desc-input-field-datalist').empty();
            $('#desc-input-field-datalist').removeClass('desc-input-field-datalist-class');
        });
        pElement.append(spanElement);
        $('#desc-input-field-datalist').append(pElement);
    });
}

function populateDetails(details, groupId) {
    let admin, sum;
    admin = false;
    for(let member of details.members) {
        if(member.is_admin === '1') {
            if(details.yourId === member.id) {
                admin = true;
            }
        }
    }
    createMemberTable(details, admin, groupId);
    sum = 0;
    for(let member of details.members) {
        sum += member.paymentsMade;
    }
    createSettlement(sum);
    leaveGroup(details, groupId, admin);
    createForm(details, groupId);
}

function toggleContent(div) {
    $('.content').each(function () {
        if ($(this).attr('id') === div) {
            $(this).show();
        } else {
            $(this).hide();
        }
    });
    $('#groups-settings').toggle();
    $('#groups-open-settings').toggleClass('groups-open-settings-open-icon');
}

function fetchDetails(groupId) {
    $.post('./group/details', {'groupId': groupId})
        .done(function (details) {
            $(document).ready(function() {
                populateDetails(details, groupId);
                getRequestJoins();
                changeGuestLogin(groupId, 'fetch');
            });
        })
        .fail(function () {
            addMessage(lang.FailedToLoadData, 'error');
        });
}

function copyToClipboard() {
    let copyText = document.getElementById('span-guest-login-url'),
        textArea = document.createElement('textarea');
    textArea.value = copyText.textContent;
    document.body.appendChild(textArea);
    textArea.select();
    textArea.setSelectionRange(0, 99999); /* For mobile devices */
    navigator.clipboard.writeText(textArea.value);
    textArea.remove();
    addMessage(lang.CopyIntoClipboard, 'info');
}

function getGroups() {
    $.getJSON('./group/get-groups')
        .done(function (response) {
            if (response.length > 0 && response !== 0) {
                $('#group-code').html(`${lang.InvitationCode}: ${response[0].code}`);
                buildAllGroups(response);
                $('#toggle-menu').toggle();
                $('#toggle-button').toggleClass('change');

                toggleGroup(response[0].gid, response[0].name, response[0].code, response[0].suggestions);

                if (response[0].isGuest) {
                    $('#menu-top').empty();
                    $('#menu-title-settings').hide();
                    $('#menu-settings').hide();
                }
            } else {
                $('#noGroup,#group').show();
                $('#details-button').hide();
                $('#add-new-user-popup,#popup-overlay,#create-new-user,#statistic-chart-div,#content-settings,#groups-header-title').hide();
                $('#loading').addClass('elem-hidden').removeClass('elem-show');
            }
            $('#add-new-user-popup,#popup-overlay,#create-new-user').hide();
        })
        .fail(function (xhr) {
            $('#groups-settlement-table,#groups-details,#groups-leave-group,#groups-create-user,#groups-create-bill-div').remove();
            $('#groups-settings-div,#statistic-chart-div,#content-settings,#loading').hide();
            $('#message-area,#menu-settings,#menu-logout,#menu-title-settings,#menu-group-join-group-content,#menu-create-popup-content').remove();
            $('#groups-header-title').text(xhr.responseText);
        })
        .always(function() {
            $('#groups-open-settings').unbind().bind('click', function() {
                $('#groups-settings').toggle();
                $('#groups-open-settings').toggleClass('groups-open-settings-open-icon');
            });
            $('#groups-details').unbind().bind('click', function () {
                toggleContent('members-table-content');
            });
            $('#groups-leave-group').unbind().bind('click', function () {
                toggleContent('groups-leave-group-content');
            });
            $('#menu-group-join-group-content').unbind().bind('click', function () {
                toggleMenuPoint();
                $('#group-join-group-content').show();
                $('#groups-header-title').html(lang.JoinGroup);
            });
            $('#menu-create-popup-content').unbind().bind('click', function () {
                toggleMenuPoint();
                $('#create-popup-content').show();
                $('#groups-header-title').html(lang.CreateGroup);
            });
            $('#groups-create-user').unbind().bind('click', function () {
                toggleContent('groups-create-user-content');
            });
            $('#groups-settlement-table').unbind().bind('click', function () {
                toggleContent('settlement-table-div');
            });
            $('#groups-create-bill').unbind().bind('click', function () {
                toggleContent('groups-create-bill-div');
            });
            $('#groups-history-button').unbind().bind('click', function () {
                updateHistory($('#groupId').val());
                toggleContent('group-history-content');
            });
            $('#groups-statistic-button').unbind().bind('click', function () {
                getStats($('#groupId').val());
                toggleContent('statistic-chart-div');
            });
            $('#groups-change-guest-login-button').unbind().bind('click', function () {
                toggleContent('div-group-change-guest-login');
            });
            $('#menu-settings').unbind().bind('click', function () {
                toggleMenuPoint();
                $('#groups-header-title').html(lang.Settings);
                toggleContent('content-settings');
                $('#content-settings').show();
                $('.settings-menu-items').each(function () {
                    $(this).show();
                });
                $('#settings-back-to-main').hide();
            });

            $('#paid-popup, #remove-user-popup,#create-user-button').hide();
        });
}

function changeGuestLogin(groupId, type) {
    if (type !== 'fetch') {
        type = $('#cbx-change-guest-login').is(':checked') ? 'enable' : 'disable';
    }
    $.post('./group/change-guest-login', {'groupId': groupId, 'cbx-change-guest-login': type})
        .done(function (details) {
            if (details['result'] === true) {
                $('#para-guest-login-url').show();
                $('#span-guest-login-url').text(details['value']);
                $('#cbx-change-guest-login').prop('checked', true);
            } else {
                $('#para-guest-login-url').hide();
                $('#span-guest-login-url').text('');
                $('#cbx-change-guest-login').prop('checked', false);
            }
            if (details['result'] === 'skip') {
                $('#groups-change-guest-login-button').hide();
                $('#groups-leave-group').hide();
                $('#groups-create-user').hide();
                $('#group-code').hide();
            }
        })
        .fail(function () {
            addMessage(lang.FailedToLoadData, 'error');
        });
}

$(document).ready(function() {
    $('#group-join-group').prop('disabled', true);
    $('#join-group-code-input').on('keyup', function () {
        if ($('#join-group-code-input').val() === 0) {
            $('#group-join-group').prop('disabled', true);
        } else {
            $('#group-join-group').prop('disabled', false);
        }
    });
    sendJoinRequest();
    $('#group-button-create-group').prop('disabled', true);
    $('#group-create-group-input').on('keyup', function () {
        if ($('#group-create-group-input').val() === 0) {
            $('#group-button-create-group').prop('disabled', true);
        } else {
            $('#group-button-create-group').prop('disabled', false);
        }
    });
    $(document).mouseup(function(e) {
        let container = $('#groups-settings-div');
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            $('#groups-settings').hide();
            $('#groups-open-settings').addClass('groups-open-settings-open-icon');
        }
    });
    $('#paid-popup-no').unbind().bind('click', function () {
        $('input:radio[name=\'mode-of-payment\']').each(function() {
            this.checked = false;
        });
    });
    $('#change-guest-login').on('click', function() {
        changeGuestLogin($('#groupId').val(), ($('#cbx-change-guest-login').is(':checked') ? 'enable' : 'disable'));
    });
    $('#btn-copy-guest-login-url').on('click', function() {
        copyToClipboard();
    });
    getGroups();

    $('#create-new-user-btn').on('click', function() {
        createNewUser();
    });
});
