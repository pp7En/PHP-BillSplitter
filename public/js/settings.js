function correctMailFormat(element) {
    let filter = /^([a-zA-Z0-9_.+-])+@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/,
        email = element.val();
    return (filter.test(email) && element.val().length >= 6);
}

function checkPasswordLengthBeforeChange() {
    let valid = true;
    valid = $('#settings-old-password').val().length >= 6 && valid;
    valid = $('#settings-new-password-first').val().length >= 6 && valid;
    valid = $('#settings-new-password-second').val().length >= 6 && valid;
    return valid;
}

function sendPostRequest(url, data, msg = '') {
    $.post(url, data)
        .done(function (request) {
            addMessage(msg === '' ? request : msg, 'info');
        })
        .fail(function (xhr) {
            addMessage(xhr.responseText, 'error');
        });
}

$(document).ready(function () {
    $.post('settings/get_settings')
        .done(function (data) {
            $('#cbx-notification').prop('checked', (data.notification === '1'));
            if (data.registration === false) {
                $('#settings-registration').remove();
            } else {
                $('#cbx-registration').prop('checked', (data.registration === '1'));
                $('#settings-registration').show();
            }
            $('#settings-new-name').val(data.name);
            $('#settings-new-mail-address').val(data.email);
        })
        .fail(function (xhr) {
            console.log(`Error: ${xhr}`);
        });
    $('#settings-password-change-button').prop('disabled', true);

    $('#yes-i-am-sure').click(function() {
        if ($(this).is(':checked') && ($('#settings-delete-account-password').val().length >= 6)) {
            $('#settings-delete-account-button').prop('disabled', false);
        } else {
            $('#settings-delete-account-button').prop('disabled', true);
        }
    });
    $('#settings-delete-account-password').bind('change keyup input', function () {
        if ($('#settings-delete-account-password').val().length >= 6) {
            $('#delete-account-password-info').text('');
        } else {
            $('#delete-account-password-info').text(lang.PasswordToShort);
        }
        if ($('#yes-i-am-sure').is(':checked') && ($('#settings-delete-account-password').val().length >= 6)) {
            $('#settings-delete-account-button').prop('disabled', false);
        } else {
            $('#settings-delete-account-button').prop('disabled', true);
        }
    });
    function toggleContent(div) {
        $('.content').each(function () {
            if ($(this).attr('id') === div) {
                $(this).show();
            } else {
                $(this).hide();
            }
        });
        $('.settings-menu-items').each(function () {
            $(this).hide();
        });
        $('#content-settings').show();
        $('#settings-back-to-main').show();
    }
    $('#settings-notification').click(function () {
        toggleContent('content1');
    });
    $('#settings-profile').click(function () {
        toggleContent('content2');
    });
    $('#settings-password').click(function () {
        toggleContent('content3');
    });
    $('#settings-delete').click(function () {
        toggleContent('content4');
    });
    $('#settings-registration').click(function () {
        toggleContent('content5');
    });

    $('#settings-back-to-main').on('click', function() {
        $('.settings-menu-items').each(function () {
            $(this).show();
            $('#settings-back-to-main').hide();
        });
        $('.content').each(function () {
            $(this).hide();
        });
        $('#content-settings').show();
    });

    $('#settings-old-password').keyup(function() {
        if ($('#settings-old-password').val().length >= 6) {
            $('#old-password-info').text('');
            if (checkPasswordLengthBeforeChange()) {
                $('#settings-password-change-button').prop('disabled', false);
            }
        } else {
            $('#old-password-info').text(lang.PasswordToShort);
            $('#settings-password-change-button').prop('disabled', true);
        }
    });

    $('#settings-new-password-first').keyup(function() {
        if ($('#settings-new-password-first').val().length >= 6) {
            $('#new-password-first-info').text('');
            if (checkPasswordLengthBeforeChange()) {
                $('#settings-password-change-button').prop('disabled', false);
            }
        } else {
            $('#new-password-first-info').text(lang.PasswordToShort);
            $('#settings-password-change-button').prop('disabled', true);
        }
    });

    $('#settings-new-password-second').keyup(function() {
        if ($('#settings-new-password-second').val().length >= 6) {
            let pass = $('#settings-new-password-first').val(),
                repass = $('#settings-new-password-second').val();
            if (pass !== repass) {
                $('#new-password-second-info').text(lang.PasswordsDoesNotMatch);
                $('#settings-password-change-button').prop('disabled', true);
            } else {
                $('#new-password-second-info').text('');
                if (checkPasswordLengthBeforeChange()) {
                    $('#settings-password-change-button').prop('disabled', false);
                }
            }
        } else {
            $('#settings-password-change-button').prop('disabled', true);
        }
    });

    $('#settings-delete-account-button').on('click', function() {
        $.post('./settings/delete_account', {
            'settings-delete-account-password': $('#settings-delete-account-password').val()
        }, function() {
            window.location.replace('./');
        })
            .done(function (request) {
                addMessage(request, 'info');
            })
            .fail(function (xhr) {
                addMessage(xhr.responseText, 'error');
            });
    });

    $('#settings-registration-button').on('click', function() {
        sendPostRequest(
            './settings/set_registration',
            {
                'cbx-registration': ($('#cbx-registration').is(':checked') ? 1 : 0)
            },
            lang.ThereWasNoError
        );
    });

    $('#settings-notification-button').on('click', function() {
        sendPostRequest(
            './settings/set_notification',
            {
                'cbx-notification': ($('#cbx-notification').is(':checked') ? 1 : 0)
            },
            lang.ThereWasNoError
        );
    });

    $('#settings-update-button').on('click', function() {
        sendPostRequest(
            './settings/details',
            {
                'name': $('#settings-new-name').val(),
                'email': $('#settings-new-mail-address').val()
            }
        );
    });

    $('#settings-password-change-button').on('click', function() {
        $.post(
            './settings/password',
            {
                'oldpass': $('#settings-old-password').val(),
                'password-first': $('#settings-new-password-first').val(),
                'password-second': $('#settings-new-password-second').val()
            }, function() {
                window.location.replace('./');
            }
        )
            .done(function (request) {
                addMessage(request, 'info');
            })
            .fail(function (request) {
                addMessage(request, 'error');
            });
    });
});
