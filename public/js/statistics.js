function getStats(groupId) {
    $.post('./statistics/statistics.json', {'groupId': groupId})
        .done(function (history) {
            if (history === null || history.length === 0) {
                if (history === null) {
                    messageNoGroup();
                } else {
                    addMessage(lang.NoPaymentHistory);
                    $('#statistic-chart-div').empty();
                }
                return;
            }
            $('#statistic-chart-div').empty();

            let chartDiv = document.getElementById('statistic-chart-div');
            chartDiv.textContent = '';

            let chart = document.createElement('canvas');
            chart.id = 'myChart';
            chartDiv.appendChild(chart);

            let ctx = document.getElementById('myChart').getContext('2d');
            new Chart(ctx, {
                'type': 'bar',
                'data': {
                    'labels': Object.keys(history),
                    'datasets': [
                        {
                            'label': '',
                            'type': 'bar',
                            'data': Object.values(history),
                            'borderWidth': 1,
                            'backgroundColor': '#4caf50'
                        }
                    ]
                },
                'options': {
                    'scales': {
                        'yAxes': [
                            {
                                'ticks': {
                                    'beginAtZero': true
                                }
                            }
                        ]
                    },
                    'legend': {
                        'display': false,
                    },
                }
            });

            let tablearea = document.getElementById('statistic-chart-div'),
                table = document.createElement('table');
            table.classList.add('customTable');

            for (const [key, value] of Object.entries(history).reverse()) {
                let tr = document.createElement('tr');
                tr.appendChild(document.createElement('td'));
                tr.appendChild(document.createElement('td'));

                tr.cells[0].appendChild(document.createTextNode(`${key.split('-')[1]}/${key.split('-')[0]}`));
                let formattedValue = value.replace('.', ',');
                tr.cells[1].appendChild(document.createTextNode(`${formattedValue} €`));
                tr.cells[1].classList.add('td-right');
                table.appendChild(tr);
            }

            tablearea.appendChild(table);
        })
        .fail(function () {
            addMessage(lang.FailedToFetchHistoryData, 'error');
        });
}
