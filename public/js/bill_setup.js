const AmountRegex = /^(\d{1,3}(\d{3})*\d{3}|\d+)(,\d\d)?$/;

let propTracker = {};

function validateFields() {
    let checkboxLength, descInputField, currValue, isValidCurrency;

    checkboxLength = $('[name="splitwith[]"]:checkbox:checked').length;
    descInputField = $('#desc-input-field').val().length;
    currValue = $('#amount-input-field').val();
    isValidCurrency = false;

    if(currValue.length > 0 && !AmountRegex.test(currValue)) {
        $('#amount-input-field-error').addClass('elem-show-inline').removeClass('elem-hidden');
        $('#amount-input-field-error').text(lang.InputValidationError);
    } else {
        $('#amount-input-field-error').addClass('elem-hidden').removeClass('elem-show-inline');
        isValidCurrency = true;
    }
    if(currValue.length > 0) {
        $('#control-label-amount-input-field').addClass('active-input-field');
    } else {
        $('#control-label-amount-input-field').removeClass('active-input-field');
    }

    if(checkboxLength === 0 || descInputField === 0 || !isValidCurrency) {
        $('#bill-create-button-add-bill').prop('disabled', true);
    } else {
        $('#bill-create-button-add-bill').prop('disabled', false);
    }
}

function initCalculator() {
    if(lang.Language === 'de') {
        $('#calc-delimiter').html(',');
    } else {
        $('#calc-delimiter').html('.');
    }
    $('#calculator-apply').click(function() {
        $('#amount-input-field').val($('#output').html());
        $('#div-calculator').toggle();
        $('#amount-input-field-error').val('');

        validateFields();
        updateValues();
    });
    $('#calculator-cancel').click(function() {
        $('#div-calculator').hide();
    });
    $('#btn-calculator').click(function() {
        $('#div-calculator').toggle();
        $('html, body').animate({
            'scrollTop': $('#amount-input-field').offset().top
        }, 200);
    });

    let screen = document.getElementById('output'),
        buttons = document.querySelectorAll('[data-option="calc-button"]');
    for (let item of buttons) {
        item.addEventListener('click', (e) => {
            let screenValue = $('#calculator-hidden').val(),
                buttonText = e.target.dataset.value;
            if(buttonText === 'C') {
                screenValue = '';
                screen.innerText = screenValue;
            } else if (buttonText === 'E') {
                screenValue = screenValue.slice(0, -1);
                screen.innerText = screenValue;
            } else if (buttonText === '=') {
                screen.innerText = parseFloat(calculate(parseCalculationString(screenValue))).toFixed(2);
            } else if (buttonText === ',' || buttonText === '.') {
                screenValue += buttonText.replace(',', '.');
                screen.innerText = screenValue;
            } else {
                screenValue += buttonText;
                screen.innerText = screenValue;
            }
            if(lang.Language === 'de') {
                screen.innerText = screen.innerText.replace(/\./g, ',');
            }
            $('#calculator-hidden').val(screenValue);
        });
    }
}

function parseCalculationString(s) {
    let calculation = [],
        current = '';
    for (let i = 0; i < s.length; i++) {
        let ch = s.charAt(i);
        if('^*/+-'.indexOf(ch) > -1) {
            if(current === '' && ch === '-') {
                current = '-';
            } else {
                calculation.push(parseFloat(current), ch);
                current = '';
            }
        } else {
            current += s.charAt(i);
        }
    }
    if(current !== '') {
        calculation.push(parseFloat(current));
    }
    return calculation;
}

function calculate(calc) {
    let ops = [
            {'*': (a, b) => a * b, '/': (a, b) => a / b},
            {'+': (a, b) => a + b, '-': (a, b) => a - b}
        ],
        newCalc = [],
        currentOp;
    for (let op of ops) {
        for (let c of calc) {
            if(op[c]) {
                currentOp = op[c];
            } else if(currentOp) {
                newCalc[newCalc.length - 1] =
                currentOp(newCalc[newCalc.length - 1], c);
                currentOp = null;
            } else {
                newCalc.push(c);
            }
        }
        calc = newCalc;
        newCalc = [];

    }
    if(calc.length > 1) {
        console.log('Error: unable to resolve calculation');
        return isNaN(calc) ? 'Error' : calc;
    } else {
        return isNaN(calc[0]) ? 'Error' : calc[0];
    }
}

function checkPercentage() {
    let runningTotal = 0;
    for (let id in propTracker) {
        runningTotal += parseInt(propTracker[id].percent.val()) || 0;
    }
    if(runningTotal !== 100) {
        $('#split-error').text(`${lang.PercentagesDontAddUpTo100ItIsCurrently} ${runningTotal}%`);
    } else {
        $('#split-error').text('');
    }
}

function updateValues() {
    let form = $('#bill-form'),
        hTotal = form[0].total.value.replace(',', '.'),
        total = parseFloat(hTotal) || 0;
    for (let id in propTracker) {
        let prop = propTracker[id].percent.val();
        propTracker[id].owed.textContent = `${(total * (prop / 100)).toFixed(2).replace('.', ',')} ${lang.Currency}`;
    }
    checkPercentage();
    validateFields();
}

function createForm(details, groupId) {
    let form, now, day, month, today;
    form = $('#bill-form');
    if(details === null) {
        messageNoGroup();
        form.hide();
        return;
    }

    $('#group-id-new-bill').val(groupId);

    generateForm(details['yourId'], details.members);

    $('#title-who-paid').html(lang.UserWhoPaid);
    $('#title-who-has-to-pay').html(lang.UserWhoHasToPay);
    $('#bill-create-button-add-bill').prop('disabled', true);
    $('#amount-input-field-error').addClass('elem-hidden');

    now = new Date();
    day = (`0${now.getDate()}`).slice(-2);
    month = (`0${now.getMonth() + 1}`).slice(-2);
    today = `${now.getFullYear()}-${month}-${day}`;

    $('#date-input-field').val(today);
}

function createPaidByUserListbox(member, yourId) {
    if(member.id === yourId) {
        $('#paid-by-user-listbox').append($('<option>').text(member.name).val(member.id).prop('selected', true));
    } else {
        $('#paid-by-user-listbox').append($('<option>').text(member.name).val(member.id));
    }
}

function functionPercentInput(e) {
    updateValues();
    let regex = /^\d+$/,
        key = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if(!regex.test(key)) {
        e.preventDefault();
        return false;
    }

    if(e.target.value.length > 1) {
        e.target.value = e.target.value.slice(0, 1);
    }
    validateFields();
}

function updateProportions(form) {
    let length, prop, runningTotal, last;

    length = Object.keys(propTracker).length;
    if(length === 0) {
        form.find('input[type="submit"]')[0].disabled = true;
        return;
    }

    form.find('input[type="submit"]')[0].disabled = false;
    prop = parseInt(100 / length);
    runningTotal = 0;
    last = null;

    for (let id in propTracker) {
        runningTotal += prop;
        last = propTracker[id];
        last.percent.val(prop);
    }

    if(runningTotal !== 100) {
        last.percent.val(prop + (100 - runningTotal));
    }

    updateValues();
}

function funcCreateBill(form, fElem, e) {
    e.preventDefault();
    if(!fElem.desc.value) {
        alert(lang.DescriptionCantBeEmpty);
        return;
    }
    if(!fElem.total.value || parseFloat(fElem.total.value) < 0.01) {
        alert(lang.TotalCantBeLessThan);
        return;
    }
    if(Object.keys(propTracker).length === 0) {
        alert(lang.MustSelectAtLeastOnePersonToSplitWith);
        return;
    }
    $.post(fElem.action, form.serializeArray())
        .done(function () {
            addMessage(lang.SuccessfullyCreatedTheBill, 'conf');
            form.find('input[type="text"], input[type="number"]').val('');
            form.find('input[type="checkbox"]').prop('checked', 'true').trigger('change');
            $('#amount-input-field').val(null);
            $('#desc-input-field-datalist').empty();
            $('#desc-input-field-datalist').removeClass('desc-input-field-datalist-class');
            getGroups();
        })
        .fail(function (ex) {
            let str = JSON.parse(ex.responseText);
            addMessage(str['error']['message'], 'error');
        })
        .always(function () {
            updateValues();
            $('html, body').animate({
                'scrollTop': $('body').offset().top
            }, 200);
        });
}

function generateForm(yourId, members) {
    let form = $('#bill-form'),
        split = $('#split-table tbody')[0];
    $('#split-table tbody').empty();
    $('#paid-by-user-listbox').empty();
    propTracker = {};

    for (let member of members) {
        let row = split.insertRow(-1),
            labelContainer = $('<label>', {'class': 'b-contain'}),
            enableInput = $('<input>', {'type': 'checkbox', 'name': 'splitwith[]', 'checked': true, 'value': member.id}),
            spanUser = $('<span>'),
            divInput = $('<div>', {'class': 'b-input'}),
            percentInput,
            owed;
        spanUser.text(member.name);
        enableInput.appendTo(labelContainer);
        spanUser.appendTo(labelContainer);
        divInput.appendTo(labelContainer);

        createPaidByUserListbox(member, yourId);

        $(row.insertCell(-1)).append(labelContainer);

        percentInput = $('<input>', {'type': 'number', 'name': 'proportions[]', 'min': 1, 'max': 100, 'class': 'percentInputField'});
        $(row.insertCell(-1)).append(percentInput);

        owed = row.insertCell(-1);
        propTracker[member.id] = {
            'percent': percentInput,
            'owed': owed
        };

        percentInput.on('keypress change input', function (e) {
            functionPercentInput(e);
        });

        enableInput.change(function (event) {
            if(event.target.checked) {
                percentInput[0].disabled = false;
                propTracker[member.id] = {
                    'percent': percentInput,
                    'owed': owed
                };
            } else {
                percentInput[0].disabled = true;
                percentInput.val('');
                owed.textContent = '';
                delete propTracker[member.id];
            }
            updateProportions(form);
            validateFields();
        });
        $('#loading').addClass('elem-hidden').removeClass('elem-show').removeClass('elem-show');
    }

    $(function() {
        $('input[type=\'checkBox\']').change(function() {
            validateFields();
        });
        $('#paid-by-user-listbox').on('change', function() {
            validateFields();
        });
    });

    updateProportions(form);

    $('#bill-create-button-add-bill').unbind().bind('click', function(e) {
        funcCreateBill(form, form[0], e);
    });
}

$(document).ready(function() {
    initCalculator();

    $('#amount-input-field').bind('change input', function () {
        validateFields();
        updateProportions($('#bill-form'));
    });

    $('#desc-input-field').focusin(function() {
        $('#desc-input-field-datalist').show();
    });
});
