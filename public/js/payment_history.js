function updateHistory(groupId) {
    $('#history-div').empty();
    $('#content-loading-area').show();

    $.post('./payment/history.json', {'groupId': groupId})
        .done(function (response) {
            if (response.length > 0) {
                handleData(response, groupId);
            } else {
                $('#group-history-content-search-area').hide();
                $('#content-loading-area').text(lang.NoPaymentHistory);
            }
        })
        .fail(function () {
            $('#content-loading-area').hide();
            addMessage(lang.FailedToFetchHistoryData, 'error');
        });
}

function buildAllGroups(response) {
    $('#group-name').append($('<select>').attr({'name': 'group-listbox', 'id': 'group-listbox'}));
    for(let i = 0; i < response.length; i++) {
        $('#group-listbox').append($('<option>').text(response[i].name).val(response[i].gid));
    }
}

function handleData(history, gid) {
    let divHighlight;
    if (history === null || history.length === 0) {
        if (history === null) {
            messageNoGroup();
        } else {
            $('#history-div').empty();
        }
        return;
    }
    $('#history-div').empty();

    const options = {'weekday': 'long', 'year': 'numeric', 'month': '2-digit', 'day': '2-digit', 'hour': '2-digit', 'minute': '2-digit'};

    for (let i = 0; i < history.length; i++) {
        let payment = history[i],
            billId = payment.billId,
            desc = payment.description,
            groupId = gid,
            prettyPrintUserName = payment.username === 'Ghost' ? lang.GhostUser : payment.username,
            date = new Date(payment.date);

        if (i % 2 === 0) {
            divHighlight = 'payment-dark';
        } else {
            divHighlight = 'payment-light';
        }

        $('#history-div').append(`<div class="payment-frame ${divHighlight}"></div>`);
        $('.payment-frame:last').append('<div class="div-history-title"></div>');
        $('.div-history-title:last').append(`<div class="${divHighlight} div-history-title-left">${payment.description}</div>`);
        $('.div-history-title:last').append(`<div class="${divHighlight} div-history-title-right"></div></a>`);

        $('.payment-frame:last').append(`<div class="${divHighlight}">${lang.HistoryTotal}: ${(parseFloat(payment.total)).toFixed(2).replace('.', ',')} ${lang.Currency}</div>`);
        $('.payment-frame:last').append(`<div class="${divHighlight}">${lang.HistoryYourPart}: ${(parseFloat(payment.quantityPaid)).toFixed(2).replace('.', ',')} ${lang.Currency}</div>`);

        $('.payment-frame:last').append(`<div class="${divHighlight}">${lang.PaidBy}: ${prettyPrintUserName}</div>`);
        $('.payment-frame:last').append(`<div class="${divHighlight}">${date.toLocaleDateString('de-DE', options)}</div>`);

        $('.div-history-title-right:last').bind('click', function () {
            let deletePopup = $('#delete-popup').popup({
                'open' () {
                    $('#para-delete-payment').html(`${lang.PopupHistoryDeletePayment}<br>${desc}`);
                }
            });
            deletePopup.element.submit(function (event) {
                event.preventDefault();
                $.ajax({
                    'url': './payment/delete-payment',
                    'type': 'POST',
                    'data': {'billId': billId, 'groupId': groupId},
                    'success'() {
                        deletePopup.hide();
                        addMessage(lang.PaymentDeleted, 'info');
                        location.reload();
                    },
                    'fail' () {
                        deletePopup.hide();
                        addMessage(lang.ThereWasAnError, 'error');
                    }
                });
            });
            $('#delete-payment-no').click(function() {
                deletePopup.hide();
            });
            deletePopup.show();
            $('#delete-popup').show();
        });
    }
    $('#content-loading-area').hide();
}
