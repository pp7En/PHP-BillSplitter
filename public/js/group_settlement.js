function createPaidPopup(fromid, toid, value) {
    let paidPopup = $('#paid-popup').popup({
        'open' () {
            $('#para-paid-popup').html(`${lang.QuestionPaidPayment}<br>`);
            $('#paid-popup-yes').prop('value', lang.ConfirmPayment);
            $('#paid-popup-yes').prop('disabled', true);
            $('#paid-popup-no').prop('value', lang.Cancel);

            $('#control-label-instalment').text('');
            $('#amount-of-partly-payment').prop('disabled', true);
            $('#paid-popup-yes').prop('disabled', true);

            $('#paid-popup').change(function() {
                if($('input[name=\'mode-of-payment\']:checked').val() === 'full') {
                    $('#control-label-instalment').text('');
                    $('#amount-of-partly-payment').prop('disabled', true);
                    $('#paid-popup-yes').prop('disabled', false);
                } else if ($('input[name=\'mode-of-payment\']:checked').val() === 'partly' && (!$('#amount-of-partly-payment').val() > 0)) {
                    $('#control-label-instalment').text(lang.AmountOfPartlyPayment);
                    $('#amount-of-partly-payment').prop('disabled', false);
                    $('#amount-of-partly-payment').focus();
                    $('#paid-popup-yes').prop('disabled', true);
                }
            });
            $('#amount-of-partly-payment').on('keyup', function () {
                if($('#amount-of-partly-payment').val() > 0 && ($('input[name=\'mode-of-payment\']:checked').val() === 'full') || $('input[name=\'mode-of-payment\']:checked').val() === 'partly') {
                    $('#paid-popup-yes').prop('disabled', false);
                } else {
                    $('#paid-popup-yes').prop('disabled', true);
                }
            });
        }
    });
    paidPopup.element.submit(function (event) {
        event.preventDefault();
        let groupid = $('#groupId').val(),
            modeOfPayment = $('input[name="mode-of-payment"]:checked').val(),
            amountOfPartlyPayment = $('#amount-of-partly-payment').val();
        $.ajax({
            'url': './payment/paid',
            'type': 'POST',
            'data': {'from': fromid, 'to': toid, 'amount': value, 'groupId': groupid, 'modeOfPayment': modeOfPayment, 'amountOfPartlyPayment': amountOfPartlyPayment},
            'success'() {
                paidPopup.hide();
                location.reload();
            },
            'fail' () {
                paidPopup.hide();
                addMessage(lang.ThereWasAnError, 'error');
            }
        });
        return false;
    });
    $('#paid-popup-no').click(function() {
        paidPopup.hide();
    });
    paidPopup.show();
    $('#paid-popup').show();
}

function createTable(data) {
    $('#settlement-table-div').append('<div class="settlement-table-line"></div>');
    $('.settlement-table-line:last').append(`<div class="settlement-table-left">${data.from} ${lang.PayTo} ${data.to}:</div>`);
    $('.settlement-table-line:last').append(`<div class="settlement-table-middle">${data.value.toFixed(2).replace('.', ',')}&nbsp;${lang.Currency}</div>`);
    $('.settlement-table-line:last').append('<div class="settlement-table-right"></div>');

    $('.settlement-table-right:last').append(`<button class="button-pay">${lang.Paid}</button>`);
    $('.button-pay:last').bind('click', function () {
        createPaidPopup(data.fromid, data.toid, data.value);
    });
}

function calcData(data, payments) {
    $('#settlement-table-div').append(`<h3>${lang.WhoMustPayWhat}</h3>`);
    if((data === undefined || data === null || data.length === 0) && payments === 0) {
        $('#settlement-table-div').append(`<p class='elem-bold-green'>${lang.NoExpensesEnteredYet}</p>`);
        return;
    } else if ((data === undefined || data === null || data.length === 0) && payments > 0) {
        $('#settlement-table-div').append(`<p class='elem-bold-green'>${lang.BillsBalanced}</p>`);
        return;
    }
    for (let detail of data) {
        createTable(detail);
    }
}

function createSettlement(payments) {
    $('#settlement-table-div').empty();
    $('#settlement').empty();
    let groupId = $('#groupId').val();
    $.ajax({
        'url': './group/settlement',
        'type': 'POST',
        'data': {'groupId': groupId},
        'success'(data) {
            calcData(data, payments);
        },
        'fail' () {
            addMessage(lang.ThereWasAnError, 'error');
        }
    });
}
