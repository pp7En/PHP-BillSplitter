function buildAddUser(data, gid) {
    if (data === null || data.length === 0) {
        $('#add-user').hide();
        return;
    }
    $('#add-user').show();

    for (let newUser of data) {
        let newUserId = newUser.id,
            groupId = gid;

        $('#add-user').append('<div class="add-user-frame"></div>');
        $('.add-user-frame:last').append('<div class="div-add-user-name"></div>');
        $('.add-user-frame:last').append('<div class="div-add-user-btn-confirm"></div>');
        $('.add-user-frame:last').append('<div class="div-add-user-btn-reject"></div>');

        $('.div-add-user-name').append(`<p>${newUser.name}</p>`);
        $('.div-add-user-btn-confirm').append(`<button class="btn-add-user-confirm">${lang.AddNewUserYes}</button>`);
        $('.div-add-user-btn-reject').append(`<button class="btn-add-user-reject">${lang.AddNewUserNo}</button>`);

        $('.btn-add-user-confirm:last').bind('click', function () {
            $.post('./group/confirm-request', {'newUserId': newUserId, 'groupId': groupId})
                .done(function () {
                    location.reload();
                })
                .fail(function () {
                    addMessage(lang.ThereWasAnError, 'error');
                });
        });

        $('.btn-add-user-reject:last').bind('click', function () {
            $.post('./group/reject-request', {'newUserId': newUserId, 'groupId': groupId})
                .done(function () {
                    location.reload();
                })
                .fail(function () {
                    addMessage(lang.ThereWasAnError, 'error');
                });
        });
    }
}

function getRequestJoins() {
    let groupId = $('#groupId').val();
    $.post('./group/get-requests', {'groupId': groupId})
        .done(function (requests) {
            buildAddUser(requests, groupId);
        })
        .fail(function (xhr) {
            addMessage(xhr.responseText, 'conf');
        });
}

function sendJoinRequest() {
    $('#group-join-group').on('click', function (event) {
        event.preventDefault();

        $.post('./group/join', {'code': $('#join-group-code-input').val()})
            .done(function () {
                addMessage(lang.RequestToJoinGroupSent, 'info');
                $('#join-popup').trigger('reset');
                $('#group-join-group-content').hide();
            })
            .fail(function () {
                addMessage(lang.ThereWasAnError, 'error');
            });
    });
}
