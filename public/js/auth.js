function validateName(name, errorElem) {
    if(typeof name !== 'string' || name.length < 2) {
        errorElem.text(lang.NameMustBeAtLeast2Characters);
        return false;
    }
    errorElem.text('');
    return true;
}

function validateEmail(email, errorElem) {
    let atSymbol, dot;

    if(typeof email !== 'string') {
        errorElem.text(lang.NotAValidEmail);
        return false;
    }

    atSymbol = email.indexOf('@');
    if(atSymbol < 1) {
        errorElem.text(lang.NotAValidEmail);
        return false;
    }

    dot = email.indexOf('.');
    if(dot <= atSymbol + 2) {
        errorElem.text(lang.NotAValidEmail);
        return false;
    }

    if(dot === email.length - 1) {
        errorElem.text(lang.NotAValidEmail);
        return false;
    }

    errorElem.text('');
    return true;
}

function validatePassword(password, errorElem) {
    if(typeof password !== 'string' || password.length < 6) {
        errorElem.text(lang.PasswordMustBeAtLeast6Characters);
        return false;
    }
    errorElem.text('');
    return true;
}

function comparePassword(password, passwordSecond, errorElem) {
    if(password !== passwordSecond) {
        errorElem.text(lang.PasswordsDoesNotMatch);
        return false;
    }
    errorElem.text('');
    return true;
}

function changeLoginButton() {
    let value = validatePassword($('#password-first').val(), $('#password-error')) &&
                validateEmail($('#email').val(), $('#email-error'));
    if(value) {
        $('#login-button').prop('disabled', false);
    } else {
        $('#login-button').prop('disabled', true);
    }
}

function regularLogin() {
    $.urlParam = function(name) {
        let results = new RegExp(`[?&]${name}=([^&#]*)`).exec(window.location.href);
        if(results === null) {
            return 0;
        }
        return results[1] || 0;
    };

    $('#validation-code').val(decodeURIComponent($.urlParam('code')));
    $('#button-signup').prop('disabled', true);
    $('#login-button').prop('disabled', true);

    $('#password-first').bind('change keyup input paste', function() {
        changeLoginButton();
        validatePassword($('#password-first').val(), $('#password-error'));
    });

    $('#password-second').bind('change keyup input', function() {
        let pass = $('#password-first').val(),
            repass = $('#password-second').val();
        if(pass !== repass) {
            $('#password-error').text(lang.PasswordsDoesNotMatch);
            $('#button-signup').prop('disabled', true);
        } else {
            $('#password-error').text('');
            $('#button-signup').prop('disabled', false);
        }
    });

    $('#email').bind('change keyup input', function() {
        let filter = /^([a-zA-Z0-9_.+-])+@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/,
            email = $('#email').val();
        if($('#email').val().length === 0) {
            $('#email-error').text('');
            $('#button-signup').prop('disabled', true);
            $('#login-button').prop('disabled', true);
            return;
        }
        if($('#email').val().length < 6 || !filter.test(email)) {
            $('#email-error').text(lang.NotAValidEmail);
            $('#button-signup').prop('disabled', true);
            $('#login-button').prop('disabled', true);
            return;
        }

        if(validatePassword($('#password-first').val(), $('#password-error')) === true) {
            $('#email-error').text('');
            $('#button-signup').prop('disabled', false);
            $('#login-button').prop('disabled', false);
        } else {
            $('#button-signup').prop('disabled', true);
            $('#login-button').prop('disabled', true);
        }

    });

    $('#restore-password-email').keyup(function() {
        if($('#restore-password-email').val().length > 6) {
            let filter = /^([a-zA-Z0-9_.+-])+@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/,
                email = $('#restore-password-email').val();
            if(!filter.test(email)) {
                $('#restore-message').text(lang.NotAValidEmail);
                $('#restore-password-button').prop('disabled', true);
            } else {
                $('#restore-message').text('');
                $('#restore-password-button').prop('disabled', false);
            }
        }
    });

    $('#apply-new-password-second').keyup(function() {
        let pass = $('#apply-new-password-first').val(),
            repass = $('#apply-new-password-second').val();
        if(pass !== repass) {
            $('#apply-new-password-error').text(lang.PasswordsDoesNotMatch);
            $('#apply-new-password-button').prop('disabled', true);
        } else {
            $('#apply-new-password-error').text('');
            $('#apply-new-password-button').prop('disabled', false);
        }
    });

    $('#login-button').on('click', function (e) {
        let valid = true;
        valid = validatePassword($('#password-first').val(), $('#password-error')) && valid;
        if(!valid) {
            e.preventDefault();
            return;
        }
        $('#login-form').submit();
    });

    $('#login-form').on('change keyup input', function(e) {
        if(e.keyCode === 13) {
            let valid = true;
            valid = validatePassword($('#password-first').val(), $('#password-error')) && valid;
            if(!valid) {
                e.preventDefault();
                return;
            }
            $('#login-form').submit();
        }
    });

    $('#button-signup').on('click', function (e) {
        let valid = true;
        valid = validatePassword($('#password-first').val(), $('#password-error')) && valid;
        valid = validatePassword($('#password-second').val(), $('#password-error')) && valid;
        valid = comparePassword($('#password-first').val(), $('#password-second').val(), $('#password-error')) && valid;
        if(!valid) {
            e.preventDefault();
            return;
        }
        $('#regform').submit();
    });

    $('#details-form').on('submit', function (e) {
        let valid = true;
        valid = validateName(e.target.name.value, $('#name-error')) && valid;
        valid = validateEmail(e.target.email.value, $('#email-error')) && valid;
        if(!valid) {
            e.preventDefault();
        }
    });

    $('#settings-password-change-button').on('click', function (e) {
        let valid = true;
        valid = validatePassword($('#settings-old-password').val(), $('#old-password-error')) && valid;
        valid = validatePassword($('#settings-new-password-first').val(), $('#password-error')) && valid;
        valid = validatePassword($('#settings-new-password-second').val(), $('#password-error')) && valid;
        valid = comparePassword($('#settings-new-password-first').val(), $('#settings-new-password-second').val(), $('#password-error')) && valid;
        if(!valid) {
            e.preventDefault();
            return;
        }
        $('#password-form').submit();
    });

    $('#restore-password-form').on('submit', function (e) {
        let valid = true;
        valid = validatePassword($('#restore-password-email').val(), $('#restore-message')) && valid;
        if(!valid) {
            e.preventDefault();
            return;
        }
        $('#restore-password-form').submit();
    });

    $('#apply-new-password-button').on('click', function (e) {
        let valid = true;
        valid = validatePassword($('#apply-new-password-first').val(), $('#apply-new-password-error')) && valid;
        valid = validatePassword($('#apply-new-password-second').val(), $('#apply-new-password-error')) && valid;
        valid = comparePassword($('#password-first').val(), $('#apply-new-password-second').val(), $('#password-error')) && valid;
        if(!valid) {
            e.preventDefault();
            return;
        }
        $('#apply-new-password-form').submit();
    });

    $('#para-password-forgotten').bind('click', function () {
        $('#loginpage-restore-password').show();
        $('#loginpage-login').hide();
    });

    $('#back-to-login').bind('click', function () {
        $('#loginpage-restore-password').hide();
        $('#loginpage-login').show();
    });
}

$(document).ready(function () {
    let autologin = window.location.hash.substring(1);
    if(autologin.length > 0) {
        let tmp = atob(autologin).split(':');
        $('#gid').val(tmp[0]);
        $('#autologincode').val(tmp[1]);
        $('#guest-login-form').submit();
    } else {
        regularLogin();
    }
});
