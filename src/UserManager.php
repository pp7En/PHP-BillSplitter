<?php

class UserManager {

    public static function getName($id) {
        $db = Database::getInstance();
        $data = $db->selectSingle('name', 'users', array('id' => $id));
        return $data ? $data['name'] : null;
    }

    public static function getDetails($id) {
        $db = Database::getInstance();
        $data = $db->selectSingle('name, email, notifications', 'users', array('id' => $id));
        if (!$data) {
            return null;
        }
        return $data;
    }
}
