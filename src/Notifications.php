<?php

class Notifications {

    const REQ_JOIN_GROUP = 1;
    const NEW_BILL = 2;
    const PAYMENT_MADE = 3;
    const PAYMENT_CANCELLED = 4;
    const PAYMENT_CONFIRMED = 5;
    const PAYMENT_DENIED = 6;
    const BILL_CONFIRMED = 7;
    const GROUP_REQUEST_REJECTED = 8;

    public static function pushNotification($toUser, $message, $type) {
        $db = Database::getInstance();
        $db->insert('notifications', array(
            'receiver_id' => $toUser,
            'message' => $message,
            'type_id' => $type
        ));

        $userDetails = UserManager::getDetails($toUser);
        if ($userDetails && NOTIFICATIONS && $userDetails['notifications'] == '1') {
            self::sendMail($userDetails['email'], $message);
        }
    }

    private static function sendMail($address, $message, $subject = null) {
        $headers   = array();
        $headers[] = "MIME-Version: 1.0";
        $headers[] = "Content-type: text/plain; charset=utf-8";
        $headers[] = "Content-Transfer-Encoding: base64";
        $headers[] = "From: " . NOTIFICATIONS_SENDER;
        $headers[] = "X-Mailer: PHP/".phpversion();

        if ($subject == null) {
            $subject = $GLOBALS['MailSubjectNotifications'];
        }
        return mail($address, $subject, base64_encode($message), implode("\r\n",$headers));
    }


    public static function getNotificationsFor($userId, $minId = 0) {
        $userId = App\AuthManager::checkIfUserStillAllowed($userId);
        $db = Database::getInstance();
        $data = $db->select('message, type_id, id', 'notifications', array('receiver_id' => $userId));
        if ($data === false) {
            $data = array();
        }
        $notifications = array();
        array_walk($data, function (&$notification) use ($minId, &$notifications) {
            $notification['type'] = (int) $notification['type_id'];
            unset($notification['type_id']);
            if (($notification['id'] = (int) $notification['id']) > $minId) {
                $notifications[] = $notification;
            }
        });
        return $notifications;
    }

    public static function dismiss($userId, $notId) {
        $userId = App\AuthManager::checkIfUserStillAllowed($userId);
        Database::getInstance()->delete('notifications', array('receiver_id' => $userId, 'id' => $notId));
    }

    public static function sendMailValidation($address, $validation_code) {
        $url = ROOT . "auth?activation=" . $validation_code;
        $message = $GLOBALS['MailMessageActivateAccount'] . " " . $url;
        self::sendMail($address, $message, $GLOBALS['MailSubjectActivateAccount']);
    }

    public static function sendResetPasswordLink($address, $validation_code) {
        $url = ROOT . "auth?code=" . $validation_code;
        $message = $GLOBALS['MailMessageResetPassword'] . " " . $url;
        self::sendMail($address, $message, $GLOBALS['MailSubjectResetPassword']);
    }

    public static function getRejectedGroupNotifications($userId) {
        $userId = App\AuthManager::checkIfUserStillAllowed($userId);
        $db = Database::getInstance();

        return $db->select('id, message', 'notifications', array(
            'receiver_id' => $userId,
            'type_id' => self::GROUP_REQUEST_REJECTED));
    }

    public static function setRejectedGroupNotifications($userId, $groupName, $userMailAddress) {
        $userId = App\AuthManager::checkIfUserStillAllowed($userId);
        $db = Database::getInstance();
        $message = $GLOBALS['GroupRequestRejected1'] . $groupName . $GLOBALS['GroupRequestRejected2'];

        self::sendMail($userMailAddress, $message);

        return $db->insert('notifications', array(
            'receiver_id' => $userId,
            'message' => $message,
            'type_id' => self::GROUP_REQUEST_REJECTED));
    }

    public static function deleteNotification($id, $userId) {
        $userId = App\AuthManager::checkIfUserStillAllowed($userId);
        $db = Database::getInstance();
        return $db->delete('notifications', array('id' => $id, 'receiver_id' => $userId));
    }

}
