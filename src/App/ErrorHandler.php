<?php

namespace App;

class ErrorHandler {

    const DEBUG = DEBUG_MODE;

    public static final function exitNow($statusCode, $message = "") {
        http_response_code($statusCode);
        if (self::DEBUG) {
            if ($message instanceof \Exception) {
                throw $message;
            }
            throw new \Exception($message);
        }
        if ($message instanceof \Exception) {
            $message = $message->getMessage();
        }
        echo $message;
        exit();
    }

    public static final function exitNowWithTemplate($statusCode, $message = "") {
        $tplName = "error/{$statusCode}";
        if (!Template::templateExists($tplName)) {
            $tplName = "error/error";
        }
        echo Template::renderTemplate($tplName, array('code' => $statusCode, 'message' => $message));
        exit();
    }
}
