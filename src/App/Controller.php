<?php

namespace App;

abstract class Controller {

    protected $template;

    protected function __construct($template = null) {
        if ($template != null && !is_string($template)) {
            throw new \InvalidArgumentException("Provided template argument is not a string");
        }
        $this->template = $template;
    }

    public function handleRequest($request, $ruleMatched) {

        // Minify all CSS files, if *.css is newer than *.min.css
        $getCss = preg_grep( '/^(?!.*min.css)/', glob(CSS_DIR . "/" . "*"));
        foreach ($getCss as $css) {
            $newFilename = str_replace(".css",".min.css", $css);
            if (!file_exists($newFilename) || filemtime($newFilename) < filemtime($css)) {
                $output = Minifier::minify_css(file_get_contents($css));
                file_put_contents($newFilename, $output);
            }
        }

        // Minify all Javascript files, if *.js is newer than *.min.js
        $getJs = preg_grep( '/^(?!.*min.js)/', glob(JS_DIR . "/" . "*"));
        foreach ($getJs as $js) {
            $newFilename = str_replace(".js",".min.js", $js);
            if (!file_exists($newFilename) || filemtime($newFilename) < filemtime($js)) {
                $output = Minifier::minify_js(file_get_contents($js));
                file_put_contents($newFilename, $output);
            }
        }

        // Minify all HMTL files, if *.html is newer than *.min.html
        $getHtml = preg_grep( '/.*^(?!.*min).*html/', glob(TEMPLATE_DIR . "/" . "*"));
        $htmlFolders = glob(TEMPLATE_DIR . "/" . "*", GLOB_ONLYDIR);
        $erg = [];
        foreach ($htmlFolders as $dir) {
            $subfolder = preg_grep( '/.*^(?!.*min).*html/', glob($dir . "/" . "*"));
            array_push($erg, $subfolder);
        }
        $allHtmlFiles = array_merge($getHtml,array_merge(...$erg));
        foreach ($allHtmlFiles as $html) {
            $newFilename = str_replace(".html",".min.html", $html);
            if (!file_exists($newFilename) || filemtime($newFilename) < filemtime($html)) {
                $output = Minifier::minify_html(file_get_contents($html));
                file_put_contents($newFilename, $output);
            }
        }

        $url = parse_url($request);
        if ($url === false) {
            $this->handleError(400, "Invalid URI");
        }
        if (!$this->authorizeRequest($request)) {
            // Error message disabled. Direct redirection.
            //$this->handleError(403, "Authorization failed");
            $this->redirect('/auth/login', 303);
        }
        if (!isset($url['path']) || strpos($url['path'], $ruleMatched) !== 0) {
            $this->handleError(404, "Path doesn't match rule");
        }
        $path = substr($url['path'], strlen($ruleMatched)) ?: '';
        if ($path !== '' && $path[0] !== '/') {
            ErrorHandler::exitNow(404, "Unknown action");
        }
        $parts = explode('/', substr($path, 1) ?: '', 2);
        if (count($parts) < 2) {
            $parts[] = '';
        }
        list($action, $args) = $parts;
        try {
            $this->handleAction($action, $args);
        } catch (\Exception $e) {
            $this->fail($e);
        }
    }

    protected function authorizeRequest($request) {
        return true;
    }

    protected function handleAction($action, $args) {
        if ($action === '') {
            $this->handleDefaultAction();
            return true;
        }
        return false;
    }

    protected function isGet() {
        return strtoupper($_SERVER['REQUEST_METHOD']) === 'GET';
    }

    protected function isPost() {
        return strtoupper($_SERVER['REQUEST_METHOD']) === 'POST';
    }

    protected function handleDefaultAction() {
        switch (strtoupper($_SERVER['REQUEST_METHOD'])) {
            case 'GET':
                $this->handleDefaultGet();
                return true;
            case 'POST':
                $this->handleDefaultPost();
                return true;
            default:
                $this->handleError(405, "HTTP Verb \"{$_SERVER['REQUEST_METHOD']}\" unsupported");
        }
    }

    protected function handleDefaultGet() {
        if ($this->template != null) {
            $this->output($this->renderTemplate($this->template));
        } else {
            $this->ensureGet();
        }
    }

    protected function handleDefaultPost() {
        $this->handleError(405, "POST request is not supported at this endpoint");
    }

    protected function renderTemplate($template, array $vars = array()) {
        $vars = array_merge($this->getGlobalVars(), $vars);
        return Template::renderTemplate($template, $vars);
    }

    protected function getGlobalVars() {
        $userId = AuthManager::getUserId();
        $user = $userId !== null ? \UserManager::getName($userId) : null;
        return array(
            'user' => $user
        );
    }

    protected function handleError($code, $message = "") {
        ErrorHandler::exitNow($code, $message);
    }

    protected function fail(\Exception $e, $code = 500) {
        $this->handleError($code, $e);
    }

    protected function failJson(\Exception $e, $code = 500) {
        http_response_code($code);
        $this->outputJson(array('error' =>
            array('message' => $e->getMessage(), 'code' => $e->getCode(), 'type' => get_class($e))
        ));
        exit;
    }

    protected function failSimpleMessage($message, $code = 409) {
        http_response_code($code);
        $this->output($message->getMessage());
        exit;
    }

    protected function output($data) {
        echo $data;
    }

    protected function outputJson($data) {
        header('Content-Type: application/json');
        $this->output(json_encode($data));
    }

    protected function checkSuccessJson($ensureTrue, $errorMessage) {
        if ($ensureTrue !== true) {
            $this->failJson(new \Exception($errorMessage));
        }
    }

    protected function redirect($path, $code = 302) {
        if (!empty($path) && $path[0] === '/') {
            $path = ROOT . substr($path, 1);
        }
        header("Location: {$path}", true, $code);
        exit;
    }

    protected function ensureGet() {
        if (!$this->isGet()) {
            $this->handleError(405, "GET request is not supported at this endpoint");
        }
    }

    protected function validatePost($varName, $opt0 = null, $opt1 = null) {
        if (!$this->isPost()) {
            $this->handleError(405);
        }
        if (!array_key_exists($varName, $_POST)) {
            $this->handleError(400, "Missing post data '{$varName}'");
        }
        $value = $_POST[$varName];
        if ($opt0 === null) {
            $value = filter_var($value, FILTER_SANITIZE_STRING);
            return $value;
        }
        if (is_int($opt0)) {
            $minLen = $opt0;
            $maxLen = $opt1;
            if (strlen($value) < $minLen) {
                $this->handleError(400, "'{$varName}' is shorter than the required length of {$minLen}");
            }
            if (strlen($value) > $maxLen && ($maxLen != null)) {
                $this->handleError(400, "'{$varName}' is longer than the allowed length of {$maxLen}");
            }
        } elseif (is_string($opt0)) {
            if ($opt0 == "email" && !filter_var($value, FILTER_VALIDATE_EMAIL)) {
                $this->handleError(400, "'{$varName}' is not a valid email address");
            }
            if ($opt0 == "password" && strlen($value) < 6 ) {
                $this->handleError(400, "'{$varName}' is too short");
            }
            if ($opt0 == "validation" && (strlen($value) != 40 )) {
                $this->handleError(400, "'{$varName}' is not a validation code");
            }
            if ($opt0 == "mode-of-payment" && $value != "full" && $value != "partly" ) {
                $this->handleError(400, "'{$varName}' is not valid");
            }
            if ($opt0 === 'int') {
                if (!ctype_digit($value)) {
                    $this->handleError(400, "'{$varName}' is not a valid integer");
                }
                $value = (int) $value;
                if (is_int($opt1) && $value < $opt1) {
                    $this->handleError(400, "'{$varName}' is smaller than the minimum of {$opt1}");
                }
            } elseif ($opt0 === 'float') {
                $value = str_replace(",", ".", $value);
                if (!is_numeric($value) || $value != (string) (float) $value) {
                    $this->handleError(400, "'{$varName}' is not a valid float");
                }
                $value = (float) $value;
                if (is_float($opt1) && $value < $opt1) {
                    $this->handleError(400, "'{$varName}' is smaller than the minimum of {$opt1}");
                }
            } elseif ($opt0 === 'array' && !is_array($value)) {
                $this->handleError(400, "'{$varName}' is not an array: '{$value}'");
            } elseif ($opt0 === 'date') {
                try {
                    new \DateTime($value);
                } catch (\Exception $e) {
                    $this->handleError(400, "'{$varName}' is not a date");
                }
            }
        } else {
            $value = filter_var($value, FILTER_SANITIZE_STRING);
        }
        return $value;
    }

    protected function validateGet($varName) {
        if (!$this->isGet()) {
            $this->handleError(405);
        }
        $value = $_GET[$varName];
        if ($value == null || !is_string($value) || $varName != 'activation') {
            $this->handleError(400, "'{$varName}' is not a legitimate parameter");
        }
        return $value;
    }

    protected function loadModel($modelName) {
        return new $modelName(\Database::getInstance());
    }

}
