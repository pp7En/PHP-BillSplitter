<?php

namespace App;

class CustomException extends \Exception {
    public function errorMessage() {
        return $this->getMessage();
    }
}
