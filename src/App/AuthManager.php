<?php

namespace App;

class AuthManager {

    const AUTH_USERID = 'auth.userId';

    public static function isLoggedIn() {
        return self::getUserId() !== null;
    }

    public static function getUserId() {
        return SessionManager::getInstance()->get(self::AUTH_USERID);
    }

    public static function login($email, $password) {
        $db = \Database::getInstance();
        $data = $db->selectSingle('id, pass_hash, enabled, validation_code', 'users', array('email' => $email));

        $retVal = array();
        if (!$data) {
            $retVal = array('success' => false, 'messages' => array('auth' => $GLOBALS['IncorrectEmailOrPassword']));
        } else if ($data['enabled'] == 0) {
            $retVal = array('success' => false, 'messages' => array('auth' => $GLOBALS['AccountNotYetActivated']));
        } else if(!password_verify($password, $data['pass_hash'])) {
            $retVal = array('success' => false, 'messages' => array('auth' => $GLOBALS['IncorrectEmailOrPassword']));
        }

        if (!empty($retVal)) {
            return $retVal;
        }

        if ($data['validation_code'] != null || $data['validation_code'] != "") {
            $db->update('users', array('validation_code' => ''), array('id' => $data['id']));
        }

        $lastLogin = time();
        $db->update('users', array('last_login' => $lastLogin), array('id' => $data['id']));

        SessionManager::getInstance()->put(self::AUTH_USERID, (int) $data['id']);
        SessionManager::getInstance()->put('timestamp', time());
        return array('success' => true);
    }

    public static function register($name, $email, $passwordFirst, $passwordSecond) {
        $db = \Database::getInstance();
        $data = $db->selectSingle('value', 'settings', array('type' => 'registration'));

        $retVal = array();
        if (array_values($data)[0] == 0) {
            $retVal = array('success' => false, 'messages' => array('password' => $GLOBALS['RegistrationDisabled']));
        } else if ($passwordFirst != $passwordSecond) {
            $retVal = array('success' => false, 'messages' => array('password' => $GLOBALS['PasswordsDoesNotMatch']));
        }

        if (!empty($retVal)) {
            return $retVal;
        }

        $validation_code = bin2hex(random_bytes(20));
        $current_time = time(); // to delete old accounts by a separated script or sql command

        $pass_hash = password_hash($passwordFirst, PASSWORD_BCRYPT, array('cost'=>12));
        if (!$db->insert('users', array('name' => $name, 'pass_hash' => $pass_hash, 'email' => $email, 'enabled' => 0, 'validation_code' => $validation_code, 'registered' => $current_time))) {
            if ($db->count('users', array('email' => $email)) !== 0) {
                $retVal = array('success' => false, 'messages' => array('email' => $GLOBALS['EmailAddressAlreadyExists']));
            } else {
                $retVal = array('success' => false, 'messages' => array());
            }
            return $retVal;
        }
        \Notifications::sendMailValidation($email, $validation_code);
        return array('success' => true, 'messages' => array('sended' => $GLOBALS['ActivationMailSended']));
    }

    public static function logout() {
        SessionManager::getInstance()->remove(self::AUTH_USERID);
    }

    public static function changeDetails($name, $email) {
        if (!self::isLoggedIn()) {
            throw new CustomException($GLOBALS['MustBeLoggedIn']);
        }
        $db = \Database::getInstance();
        return $db->update('users', array('name' => $name, 'email' => $email), array('id' => self::getUserId()));
    }

    public static function changePassword($oldPass, $newPassFirst, $newPassSecond) {
        if (!self::isLoggedIn()) {
            return $GLOBALS['MustBeLoggedIn'];
        }

        $db = \Database::getInstance();
        $data = $db->selectSingle('id, pass_hash', 'users', array('id' => self::getUserId()));
        $retVal = null;
        if (!$data) {
            $retVal = false;
        } else if (!password_verify($oldPass, $data['pass_hash'])) {
            $retVal = $GLOBALS['IncorrectOldPassword'];
        } else if ($newPassFirst != $newPassSecond) {
            $retVal = $GLOBALS['NewPasswordsDoesNotMatch'];
        }

        if (!empty($retVal)) {
            return $retVal;
        }

        $newPassHash = password_hash($newPassFirst, PASSWORD_BCRYPT, array('cost'=>12));
        return $db->update('users', array('pass_hash' => $newPassHash), array('id' => self::getUserId()));
    }

    public static function validateAccount($validation_code) {
        $pattern = "/[a-z0-9]{40}/";
        if (!preg_match($pattern, $validation_code)) {
            throw new CustomException($GLOBALS['NotAValidationCode']);
        }
        $db = \Database::getInstance();
        $data = $db->selectSingle('id, validation_code', 'users', array('validation_code' => $validation_code));
        if (!$data) {
            throw new CustomException($GLOBALS['NotAValidationCode']);
        }
        $db->update('users', array('enabled' => 1, 'validation_code' => "", 'status' => 2), array('id' => $data['id']));
        return $GLOBALS['AccountActivated'];
    }

    public static function deleteAccount($userId, $password) {
        $userId = self::checkIfUserStillAllowed($userId);
        try {
            if (!self::isLoggedIn()) {
                throw new CustomException($GLOBALS['MustBeLoggedIn']);
            }

            $db = \Database::getInstance();
            $data = $db->selectSingle('id, pass_hash', 'users', array('id' => self::getUserId()));
            if (!$data) {
                throw new CustomException($GLOBALS['UnknownErrorOccurred']);
            }

            if (!password_verify($password, $data['pass_hash'])) {
                throw new CustomException($GLOBALS['IncorrectOldPassword']);
            }

            $data = $db->querySingle('SELECT SUM(balance) AS balance FROM payments WHERE user_id = ?', array($userId));
            $balance = number_format($data['balance'],2);
            if ($balance !== '0.00') {
                throw new CustomException($GLOBALS['AccountUnbalanced']);
            }

            $data = $db->querySingle('SELECT count(*) AS quantity FROM mapping WHERE uid = ? AND is_admin = 1', array(self::getUserId()));
            if((int)$data['quantity'] >= 1) {
                throw new CustomException($GLOBALS['DetermineNewAdmin']);
            }

            $data = $db->delete('users', array('id' => self::getUserId()));
            if(!$data) {
                throw new CustomException($GLOBALS['UnknownErrorOccurred']);
            }

            $_SESSION = array();
            session_destroy();
            return true;

        } catch (CustomException $e) {
            return $e->errorMessage();
        }
    }

    public static function setNotification($value) {
        if (!self::isLoggedIn()) {
            throw new CustomException($GLOBALS['MustBeLoggedIn']);
        }
        $db = \Database::getInstance();
        $data = $db->update('users', array('notifications' => $value), array('id' => self::getUserId()));
        if (!$data) {
            return false;
        }
        return true;
    }

    public static function getNotification() {
        if (!self::isLoggedIn()) {
            throw new CustomException($GLOBALS['MustBeLoggedIn']);
        }
        $db = \Database::getInstance();
        $data = $db->selectSingle('notifications', 'users', array('id' => self::getUserId()));
        return (array_values($data)[0]);
    }

    public static function passwordReset($email) {
        $db = \Database::getInstance();
        $validation_code = bin2hex(random_bytes(20));
        $db->update('users', array('validation_code' => $validation_code), array('email' => $email));
        \Notifications::sendResetPasswordLink($email, $validation_code);
        return array('success' => true, 'messages' => array('sended' => $GLOBALS['ActivationMailSended']));
    }

    public static function appylNewPassword($validationCode, $passwordFirst, $passwordSecond) {
        $db = \Database::getInstance();
        $resp = $db->selectSingle('id', 'users', array('validation_code' => $validationCode));
        if(!empty($resp)) {
            if (strcmp($passwordFirst, $passwordSecond) == 0) {
                $db->update('users', array('pass_hash' => $passwordFirst, 'validation_code' => ''), array('id' => $resp['id']));
                return array('success' => true);
            } else {
                throw new CustomException($GLOBALS['PasswordsDoesNotMatch']);
            }
        }
        return array('success' => false, 'messages' => 'wrong old password');
    }

    public static function setRegistration($userId, $value) {
        $userId = self::checkIfUserStillAllowed($userId);
        $db = \Database::getInstance();
        $resp = $db->selectSingle('status', 'users', array('id' => $userId, 'status' => 3));
        if(empty($resp)) {
            throw new CustomException($GLOBALS['UnknownErrorOccurred']);
        }
        $db->update('settings', array('value' => $value), array('type' => 'registration'));
        return array('success' => true);
    }

    public static function getRegistration($userId) {
        $userId = self::checkIfUserStillAllowed($userId);
        $db = \Database::getInstance();
        $resp = $db->selectSingle('status', 'users', array('id' => $userId, 'status' => 3));
        if(is_null($resp)) {
            return false;
        }
        $data = $db->selectSingle('value', 'settings', array('type' => 'registration'));
        return (array_values($data)[0]);
    }

    public static function getRegistrationValue() {
        $db = \Database::getInstance();
        $data = $db->selectSingle('value', 'settings', array('type' => 'registration'));
        return (array_values($data)[0]);
    }

    public static function getDetails() {
        if (!self::isLoggedIn()) {
            throw new CustomException($GLOBALS['MustBeLoggedIn']);
        }
        $db = \Database::getInstance();
        return $db->selectSingle('name, email', 'users', array('id' => self::getUserId()));
    }

    public static function autoGuestLogin($gid, $hashedCode) {
        $db = \Database::getInstance();
        $data = $db->selectSingle('type', 'settings', array('type' => 'gid='.$gid, 'value' => $hashedCode));
        if (!$data) {
            ErrorHandler::exitNowWithTemplate(500, $GLOBALS['NotAValidGuestCode']);
        }
        $gid = str_replace('gid=', '', $data['type']);

        // get user id (from mapping) of group
        $guestUserId = $db->selectSingle('uid', 'mapping', array('gid' => $gid, 'status' => 9));
        if (empty($guestUserId)) {
            ErrorHandler::exitNowWithTemplate(500, $GLOBALS['UnknownErrorOccurred']);
        }
        SessionManager::getInstance()->put('auth.userId', (int) $guestUserId['uid']);

        return array('success' => true);
    }

    public static function isGuestUser($userId) {
        $db = \Database::getInstance();
        $data = $db->selectSingle('status', 'users', array('id' => $userId));
        if ($data['status'] == 9) {
            return true;
        }
        return false;
    }

    public static function checkIfUserStillAllowed($userId) {
        $db = \Database::getInstance();
        $userId = $db->selectSingle('id', 'users', array('id' => $userId));
        if (empty($userId) || $userId == null || $userId == "") {
            session_destroy();
            throw new CustomException($GLOBALS['UnknownUserID']);
        }
        return $userId['id'];
    }
}
