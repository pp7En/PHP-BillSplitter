<?php

namespace App;

class Template {

    public static function renderTemplate($templateFile, array $subsititions = array()) {
        $template = new Template($templateFile, $subsititions);
        return $template->render();
    }

    public static function templateExists($name) {
        return file_exists(TEMPLATE_DIR . "/{$name}.html");
    }

    private $file;
    private $vars;
    private $output;

    private function __construct($file, $vars) {
        $this->file = TEMPLATE_DIR . "/{$file}.html";
        if (!file_exists($this->file)) {
            throw new \UnexpectedValueException("Template file doesn't exist! {$this->file}");
        }
        if (is_null(LANGUAGE)) {
            throw new \UnexpectedValueException("LANGUAGE constant missing! Check your config file!");
        }
        $this->vars = $vars;
    }

    public function render() {
        if ($this->output !== null) {
            return $this->output;
        }
        $content = file_get_contents($this->file);

        $build = file_get_contents('build');
        $build = trim($build);

        try {
            $dt = \DateTime::createFromFormat('Y-m-d H:i:s', $build);
            $newBuildString = $dt->format('YmdHi');
        } catch (\Error $e) {
            $now = new \DateTime();
            $newBuildString = $now->format('YmdHi');
        }

        $languagePath = ROOT . "i18n/lang_" . LANGUAGE . ".js?v=" . $newBuildString;
        $content = str_replace('{% language %}', $languagePath, $content);

        $content = str_replace('.css?version %}', '.css?v=' . $newBuildString . " %}", $content);
        $content = str_replace('.js?version %}', '.js?v=' . $newBuildString . " %}", $content);
        $content = str_replace('{% build %}', 'Build: ' . $build, $content);


        if ($this->file === 'view/layout_unauth.html') {
            $db = \Database::getInstance();
            $data = $db->selectSingle('value', 'settings', array('type' => 'registration'));
            if ($data['value'] == 1) {
                $content = str_replace('{% auth-register %}', 'elem-show', $content);
            } else {
                $content = str_replace('{% auth-register %}', 'elem-hidden', $content);
            }
        }


        return $this->output = $this->replaceTags($content);
    }

    private function replaceTags($content) {
        $content = preg_replace_callback('/[\r\n]?\{#\s*(.+?)\s*#\}[\r\n]?/s', array($this, 'handleReplace'), $content);
        $content = preg_replace_callback('/[\r\n]?\{%\s*(.+?)\s*%\}[\r\n]?/s', array($this, 'handleReplace'), $content);
        return $content;
    }

    private function handleReplace(array $match) {
        $instruction = $match[1];
        $retVal = '';
        if (preg_match('/^url (.+)/', $instruction, $urlMatch)) {
            $url = $urlMatch[1];
            if ($url[0] === '/') {
                $url = substr($url, 1);
            }
            $retVal = ROOT . $url;
        } else if (preg_match('/^include (.+)/', $instruction, $incMatch)) {
            $retVal = self::renderTemplate($incMatch[1], $this->vars);
        } else if (preg_match('/^=(\S+)/', $instruction, $varMatch)) {
            $retVal = $this->getVar($varMatch[1]);
        } else if (preg_match('/^(\S+)=(.*)/s', $instruction, $varMatch)) {
            $this->setVar($varMatch[1], $this->replaceTags($varMatch[2]));
            $retVal = '';
        }
        return $retVal;
    }

    private function sanitize($value, $modifiers) {
        if (!$modifiers) {
            return htmlspecialchars($value); // default
        }
        if ($modifiers == 'html') {
            return $value;
        }
        throw new \Execption("Unknown modifier {$modifiers}");
    }

    private function getVar($name) {
        $parts = explode('|', $name, 2);
        if (count($parts) == 1) {
            $parts[1] = '';
        }
        $modifiers = $parts[1];
        $parts = explode('.', $parts[0]);
        $obj = $this->vars;
        foreach ($parts as $part) {
            $obj = $this->getVarMethod($part, $obj);
            if ($obj === null || (!is_object($obj) && !is_array($obj))) {
                return $this->sanitize($obj, $modifiers);
            }
        }
        return null;
    }

    private function getVarMethod($part, $obj) {
        if (is_array($obj)) {
            if (array_key_exists($part, $obj)) {
                $obj = $obj[$part];
            } else {
                $obj = null;
            }
        } elseif (is_object($obj)) {
            if (isset($obj->$part)) {
                $obj = $obj->$part;
            } else {
                $obj = null;
            }
        } else {
            throw new \Execption();
        }
        return $obj;
    }

    private function setVar($name, $value) {
        $parts = explode('.', $name);
        $obj = &$this->vars;
        foreach ($parts as $i => $part) {
            if (is_array($obj)) {
                if (array_key_exists($part, $obj) && (is_object($obj[$part]) || is_array($obj[$part]))) {
                    $obj = &$obj[$part];
                } else {
                    if ($i == count($parts) - 1) {
                        $obj[$part] = $value;
                        break;
                    } else {
                        $obj[$part] = array();
                        $obj = &$obj[$part];
                    }
                }
            } elseif (is_object($obj)) {
                if (isset($obj->$part) && (is_object($obj->$part) || is_array($obj->$part))) {
                    $obj = &$obj->$part;
                } else {
                    if ($i == count($parts) - 1) {
                        $obj->$part = $value;
                        break;
                    } else {
                        $obj->$part = new StdClass();
                        $obj = &$obj->$part;
                    }
                }
            } else {
                throw new \Execption();
            }
        }
    }

}
