<?php
/*
Admin status:
0 No Admin
1 Is admin

Group status:
0 No group
1 Request sended
2 Request confirmed
9 Guest
*/
class GroupModel {

    const SUM_BALANCE = 'SUM(balance)';

    private $db;

    public function __construct(Database $db) {
        $this->db = $db;
    }

    public function getDetails($userId, $groupId) {
        $userId = App\AuthManager::checkIfUserStillAllowed($userId);
        $this->db->insertOrUpdate('settings', array('type' => 'lastGidForUserID='.$userId, 'value' => $groupId));
        $data = $this->db->querySingle(<<<SQL
            SELECT uid AS id, groups.name, owner
            FROM groups
            INNER JOIN mapping ON mapping.gid = groups.id
            INNER JOIN users ON users.id = mapping.uid
            WHERE mapping.uid = ? AND (mapping.status = 2 OR mapping.status = 9) AND mapping.gid = ?
            SQL, array($userId, $groupId));
        if ($data === false || empty($data)) {
            return null;
        }
        $members = $this->db->query(<<<SQL
            SELECT users.name, users.id, mapping.is_admin, email AS registered,
                (SELECT count(*) FROM payments WHERE user_id = users.id AND payments.status = 3 AND bill_id IS NOT NULL AND group_id = mapping.gid) AS payments_made,
                (SELECT SUM(balance) as balance FROM payments WHERE user_id = users.id AND group_id = mapping.gid) AS payments_due
            FROM users
            INNER JOIN mapping ON mapping.uid = users.id
            INNER JOIN groups ON groups.id = mapping.gid
            WHERE (mapping.status = 2) AND mapping.gid = ? AND users.id != 999999 AND users.name != 'Ghost'
            SQL, array($groupId));
        if ($members === false) {
            $members = array();
        }
        array_walk($members, function (&$user) use (&$data) {
            if ($user['registered'] == null) {
                $user['registered'] = "no";
            } else {
                $user['registered'] = "yes";
            }
            $user['paymentsMade'] = (int) $user['payments_made'];
            $user['paymentsDue'] = (float) $user['payments_due'];
            unset($user['payments_made'], $user['payments_due']);
            $user['isOwner'] = $user['id'] == $data['owner'];
        });
        return array(
            'name' => $data['name'],
            'yourId' => $userId,
            'members' => $members
        );
    }

    public function create($name, $ownerId) {
        $counter = 0;
        do {
            $joinCode = bin2hex(random_bytes(5));
            $success = $this->db->insert('groups', array('name' => $name, 'owner' => $ownerId, 'code' => $joinCode));
            $counter++;
        } while (!$success && $counter < 5);
        if (!$success) {
            return false;
        }
        $gid = $this->db->lastId();
        if(!$this->db->insert('mapping', array('uid' => $ownerId, 'gid' => $gid, 'is_admin' => 1, 'status' => 2))) {
            return false;
        }
        return $gid;
    }

    public function requestJoin($userId, $groupCode) {
        $groupId = $this->db->selectSingle('id', 'groups', array('code' => $groupCode));
        if ($groupId === false || $groupId === null) {
            throw new App\CustomException($GLOBALS['NoUserWithMailAddress']);
        }
        $check = $this->db->selectSingle('uid', 'mapping', array('uid' => $userId, 'gid' => $groupId['id']));
        if ($check || $check != null) {
            throw new App\CustomException($GLOBALS['UserAlreadyHasMembershipGroup']);
        }
        return $this->db->insert('mapping', array('uid' => $userId, 'gid' => $groupId['id'], 'is_admin' => 0, 'status' => 1));
    }

    public function getGroupMembers($userId, $groupId = null) {
        $userId = App\AuthManager::checkIfUserStillAllowed($userId);
        if ($groupId == null) {
            $groupId = $this->db->query('SELECT gid FROM mapping WHERE uid = ? AND status = 2', array($userId));
        }
        $allGroups = array();
        foreach ($groupId as &$gid) {
            $members = $this->db->query(<<<SQL
                SELECT users.id, users.name, groups.name AS groupname , groups.id AS groupid
                FROM users
                INNER JOIN mapping ON mapping.uid = users.id
                INNER JOIN groups ON groups.id = mapping.gid
                WHERE (users.status = 2 OR users.status = 3) AND (mapping.status = 2 OR mapping.status = 9) AND mapping.gid = ? AND users.id != 999999 AND users.name != 'Ghost'
                SQL, array($gid['gid']));
            if ($members != null || $members) {
                array_push($allGroups, $members);
            }
        }
        if (!$allGroups || $allGroups == null) {
            return null;
        }
        return $allGroups;
    }

    public function getRequestJoins($userId, $groupId) {
        $groupCheck = $this->db->query('SELECT gid FROM mapping WHERE (status = 2 OR status = 9) AND gid = ?', array($groupId));
        if(!$groupCheck || $groupCheck == null) {
            throw new App\CustomException($GLOBALS['YouAreNotInGroup']);
        }
        if($this->db->selectSingle('gid', 'mapping', array('uid' => $userId, 'gid' => $groupId, 'status' => 1))) {
            throw new App\CustomException($GLOBALS['WaitingForConfirmation']);
        }
        $requests = $this->db->query('SELECT users.id, users.name, users.email FROM mapping INNER JOIN users ON users.id = mapping.uid WHERE mapping.status = 1 AND mapping.gid = ?', array($groupId));
        if (count($requests) === 0) {
            return null;
        }
        return $requests;
    }

    public function getConfirmJoins($userId, $newMember, $gid) {
        $groupId = $this->db->selectSingle('gid', 'mapping', array('uid' => $userId, 'gid' => $gid, 'status' => 2));
        if (!$groupId || $groupId == null) {
            throw new App\CustomException($GLOBALS['YouAreNotInGroup']);
        }
        return $this->db->update('mapping', array('status' => 2), array('uid' => $newMember, 'gid' => $groupId['gid'], 'status' => 1));
    }

    public function rejectRequest($userId, $newMember, $groupId) {
        $groupId = $this->db->querySingle('SELECT gid FROM mapping WHERE uid = ? AND gid = ? AND status = 2', array($userId, $groupId));
        if (!$groupId || $groupId['gid'] == null) {
            throw new App\CustomException($GLOBALS['YouAreNotInGroup']);
        }
        $groupName = $this->db->querySingle('SELECT name FROM groups INNER JOIN mapping ON mapping.gid = groups.id WHERE uid = ? AND gid = ? AND status = 2', array($userId, $groupId['gid']));

        $newMemberMailAddress = $this->db->selectSingle('email', 'users', array('id' => $newMember));
        Notifications::setRejectedGroupNotifications($newMember, $groupName['name'], $newMemberMailAddress['email']);

        return $this->db->delete('mapping', array('uid' => $newMember, 'gid' => $groupId['gid'], 'status' => 1));
    }

    public function transmitGroup($oldAdmin, $newAdmin, $groupId) {
        $groupIdOldAdmin = $this->db->selectSingle('gid', 'mapping', array('uid' => $oldAdmin, 'is_admin' => 1, 'status' => 2, 'gid' => $groupId));

        // newAdmin is false if the listbox was empty (no other users in this group)
        if ($newAdmin !== false) {
            $groupIdNewAdmin = $this->db->selectSingle('gid', 'mapping', array('uid' => $newAdmin, 'is_admin' => 0, 'status' => 2, 'gid' => $groupId));
        }

        // If last user of a group
        $countUser = $this->db->querySingle('SELECT count(*) AS count FROM mapping INNER JOIN users ON users.id = mapping.uid WHERE mapping.gid = ? AND users.email IS NOT NULL', array($groupId));
        if ($countUser['count'] == 1) {
            $delGuestUsers = $this->db->query('DELETE FROM users WHERE users.email IS NULL AND users.id IN (SELECT uid FROM mapping WHERE gid = ?)', array($groupId));
            $delOldAdmin = $this->db->delete('mapping', array('gid' => $groupIdOldAdmin['gid'], 'uid' => $oldAdmin));
            $delGroup = $this->db->delete('groups', array('id' => $groupIdOldAdmin['gid'], 'owner' => $oldAdmin));
            return ($delGuestUsers && $delOldAdmin && $delGroup);
        }

        // Check balance of olduser
        $balance = $this->db->querySingle('SELECT SUM(balance) FROM payments INNER JOIN mapping ON mapping.gid = payments.group_id WHERE mapping.uid = ? AND mapping.gid = ?', array($oldAdmin, $groupId));

        // If not zero, throw exception
        if($balance[self::SUM_BALANCE] != 0.00) {
            throw new App\CustomException($GLOBALS['AccountUnbalanced']);
        }
        // If not admin and balance is null or 0.0
        if ((is_null($balance[self::SUM_BALANCE]) || $balance[self::SUM_BALANCE] == 0.00) && $groupIdOldAdmin == null) {
            // remove non-admin user
            $this->db->delete('mapping', array('gid' => $groupId, 'uid' => $oldAdmin));
            return true;
        }

        if ($groupIdOldAdmin['gid'] != $groupIdNewAdmin['gid']) {
            throw new App\CustomException($GLOBALS['DifferentGroups']);
        }
        // set new admin
        $this->db->update('mapping', array('is_admin' => 1),  array('gid' => $groupId, 'uid' => $newAdmin));
        // remove old admin
        $this->db->delete('mapping', array('gid' => $groupId, 'uid' => $oldAdmin));
        return true;
    }

    public function createNewGuestUser($userId, $guest, $groupId) {
        $userId = App\AuthManager::checkIfUserStillAllowed($userId);
        if ($guest == "" || $guest == null) {
            throw new App\CustomException($GLOBALS['GuestUserNameCannotBeEmpty']);
        }
        $group = $this->db->selectSingle('gid', 'mapping', array('uid' => $userId, 'gid' => $groupId, 'is_admin' => 1));
        if ($group == "" || $guest == null) {
            throw new App\CustomException($GLOBALS['YouAreNotAdminOfYourGroup']);
        }
        $current_time = time();
        // Add guest user and get user id
        $result = $this->db->insert('users', array('name' => $guest, 'enabled' => 1, 'registered' => $current_time, 'status' => 2));
        if (!$result) {
            throw new App\CustomException($GLOBALS['CannotAddGuestUserToUsersTable']);
        }
        $uid = $this->db->lastId();

        // Add guest user into mapping table
        $result = $this->db->insert('mapping', array('uid' => $uid, 'gid' => $groupId, 'is_admin' => 0, 'status' => 2));
        if (!$result) {
            throw new App\CustomException($GLOBALS['CannotAddGuestUserToMappingTable']);
        }
        return true;
    }

    public function removeUser($userId, $guest, $groupId) {
        $userId = App\AuthManager::checkIfUserStillAllowed($userId);
        $group = $this->db->selectSingle('gid', 'mapping', array('uid' => $userId, 'gid' => $groupId, 'is_admin' => 1));
        if ($group == "" || $guest == null) {
            throw new App\CustomException($GLOBALS['YouAreNotAdminOfYourGroup']);
        }
        $balance = $this->db->querySingle('SELECT SUM(balance) FROM payments INNER JOIN mapping ON mapping.gid = payments.group_id WHERE mapping.uid = ? AND mapping.gid = ?', array($guest, $groupId));
        if ($balance[self::SUM_BALANCE] != 0.00) {
            throw new App\CustomException($GLOBALS['AccountUnbalanced']);
        }

        $ghostExist = $this->db->selectSingle('id', 'users', array('id' => 999999));
        if (!$ghostExist) {
            $this->db->insert('users', array('id' => 999999, 'name' => 'Ghost'));
        }
        $this->db->update('bills', array('collector' => 999999), array('collector' => $guest));
        $this->db->update('mapping', array('uid' => 999999), array('uid' => $guest));
        return $this->db->delete('mapping', array('uid' => $guest, 'gid' => $groupId));
    }

    public function getAllGroups($userId) {
        $userId = App\AuthManager::checkIfUserStillAllowed($userId);
        $lastGroupId = $this->db->selectSingle('value', 'settings', array('type' => 'lastGidForUserID='.$userId));
        $lastGroupId = isset($lastGroupId['value']) ? $lastGroupId['value'] : 0;
        $groups = $this->db->query(<<<SQL
            SELECT mapping.gid, name, groups.code
            FROM mapping
            INNER JOIN groups ON groups.id = mapping.gid
            WHERE (mapping.status = 2 OR mapping.status = 9) AND uid = ?
            ORDER BY CASE WHEN mapping.gid = $lastGroupId THEN 1 ELSE 2 END, name DESC
            SQL, array($userId));

        foreach ($groups as &$detail) {
            $detail['suggestions'] = $this->db->query('SELECT description AS text FROM bills WHERE group_id = ? GROUP BY description', array($detail['gid']));
        }

        $isGuest = $this->db->selectSingle('status', 'users', array('id' => $userId));
        if ($isGuest['status'] == 9) {
            $groups[0]['isGuest'] = true;
        }

        return $groups;
    }

    public function changeGuestLoginForGroup($userId, $groupId, $type) {
        $userId = App\AuthManager::checkIfUserStillAllowed($userId);
        $group = $this->db->selectSingle('gid', 'mapping', array('uid' => $userId, 'gid' => $groupId, 'is_admin' => 1));
        if (empty($group)) {
            return array('result' => 'skip');
        }

        switch ($type) {
            case 'fetch':
                $value = $this->fetchGuestLoginForGroup($groupId);
                break;
            case 'enable':
                $value = $this->enableGuestLoginForGroup($groupId);
                break;
            case 'disable':
                $value = $this->disableGuestLoginForGroup($userId, $groupId) ;
                break;
            default:
                break;
        }

        return $value;
    }

    private function fetchGuestLoginForGroup($groupId) {
        $code = $this->db->selectSingle('value', 'settings', array('type' => 'gid='.$groupId));
        return $code == null ? array('result' => 'empty') : array('result' => true, 'value' => $this->generateAutologinLink($groupId, $code['value']));
    }

    private function enableGuestLoginForGroup($groupId) {
        $code = $this->db->selectSingle('value', 'settings', array('type' => 'gid='.$groupId));
        if (empty($code)) {
            $this->db->insert('users', array('name' => 'guest', 'status' => 9));
            $guestUserId = $this->db->lastId();
            $this->db->insert('mapping', array('uid' => $guestUserId, 'gid' => $groupId, 'status' => 9));
            $accessCode = $this->createAccessCode();
            $this->db->insert('settings', array('type' => 'gid='.$groupId, 'value' => $accessCode));
            $value = array('result' => true, 'value' => $this->generateAutologinLink($groupId, $accessCode));
        } else {
            $value = array('result' => true, 'value' => $this->generateAutologinLink($groupId, $code['value']));
        }
        return $value;
    }

    private function disableGuestLoginForGroup($userId, $groupId) {
        $userId = App\AuthManager::checkIfUserStillAllowed($userId);
        $this->db->querySingle(<<<SQL
            DELETE users, mapping
            FROM users
            INNER JOIN mapping ON mapping.uid = users.id
            WHERE mapping.gid = ? AND mapping.status = 9
        SQL, array($groupId));

        $this->db->delete('settings', array('type' => 'gid='.$groupId));

        return array('result' => 'empty');
    }

    private function createAccessCode() {
        $comb = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $shfl = str_shuffle($comb);
        return substr($shfl,0,20);
    }

    private function generateAutologinLink($groupId, $code) {
        return ROOT . '#' . base64_encode($groupId . ':' . $code);
    }
}
