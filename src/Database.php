<?php

class Database {

    public static final function getInstance() {
        static $instance;
        if ($instance === null) {
            $instance = new self();
        }
        return $instance;
    }

    private $con;

    private function __construct() {
        try {
            if(USE_SQLITE) {
                $this->con = new PDO('sqlite:' . DB_FILE);
            } else {
                $this->con = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME .';charset=utf8', DB_USER, DB_PASSWORD);
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function asPDO() {
        return $this->con;
    }

    private function failIfFalse($stmt) {
        if ($stmt === false) {
            $error = $this->con->errorInfo();
            throw new UnexpectedValueException($error[2]);
        }
    }

    // WARNING! No validation is done here. Validation will be added in the future.
    public function insert($tableName, array $data) {
        $keys = array_keys($data);
        $querys = implode(', ', array_fill(0, count($data), '?'));
        $stmt = $this->con->prepare("INSERT INTO {$tableName} (" . implode(', ', $keys) . ") VALUES ({$querys})");
        $this->failIfFalse($stmt);
        foreach ($keys as $i => $key) {
            $stmt->bindValue($i + 1, $data[$key]);
        }
        return $stmt->execute();
    }

    // WARNING! No validation is done here. Validation will be added in the future.
    public function select($columns, $table, array $where = null) {
        $sql = "SELECT {$columns} FROM {$table}";
        $stmt = $this->generateWhereStatement($sql, $where);
        if (!$stmt->execute()) {
            return false;
        }
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function selectSingle($columns, $table, array $where = null) {
        $result = $this->select($columns, $table, $where);
        if ($result === false) {
            return false;
        }
        if (empty($result)) {
            return null;
        }
        return $result[0];
    }

    // WARNING! No validation is done here. Validation will be added in the future.
    public function update($table, array $newValues, array $where = null, $affectedRows = false) {
        $keys = implode(' = ?, ', array_keys($newValues)) . ' = ?';
        $values = array_values($newValues);
        $stmt = $this->generateWhereStatement("UPDATE {$table} SET {$keys}", $where, count($newValues));
        foreach ($values as $i => $value) {
            $stmt->bindValue($i + 1, $value);
        }
        $success = $stmt->execute();

        if(!$success) {
            return false;
        }

        if ($affectedRows) {
            $stmt->rowCount();
        }
        return true;
    }

    // WARNING! No validation is done here. Validation will be added in the future.
    public function delete($table, array $where) {
        $sql = "DELETE FROM {$table}";
        $stmt = $this->generateWhereStatement($sql, $where);
        return $stmt->execute();
    }

    private function generateWhereStatement($sql, array $where = null, $whereOffset = 0) {
        $whereKeys = array();
        if ($where != null && !empty($where)) {
            $whereKeys = array_keys($where);
            $sql .= ' WHERE ' . implode(' = ? AND ', $whereKeys) . ' = ?';
        }
        $stmt = $this->con->prepare($sql);
        $this->failIfFalse($stmt);
        foreach ($whereKeys as $i => $key) {
            $stmt->bindValue($whereOffset + $i + 1, $where[$key]);
        }
        return $stmt;
    }

    // WARNING! This method is unsafe as no validation is applied to the query
    public function query($query, array $data = array(), $affectedRows = false) {
        $stmt = $this->con->prepare($query);
        $this->failIfFalse($stmt);
        foreach ($data as $i => $d) {
            $stmt->bindValue($i + 1, $d);
        }
        if (!$stmt->execute()) {
            return false;
        }
        return $affectedRows ? $stmt->rowCount() : $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function querySingle($query, array $data = array()) {
        $stmt = $this->con->prepare($query);
        $this->failIfFalse($stmt);
        foreach ($data as $i => $d) {
            $stmt->bindValue($i + 1, $d);
        }
        if (!$stmt->execute()) {
            return false;
        }
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($result === false) {
            return false;
        }
        if (empty($result)) {
            return null;
        }
        return $result[0];
    }

    // WARNING! No validation is done here. Validation will be added in the future.
    public function count($table, array $where) {
        $stmt = $this->generateWhereStatement("SELECT count(*) FROM {$table}", $where);
        if (!$stmt->execute()) {
            return false;
        }
        return (int) $stmt->fetchColumn();
    }

    public function lastId($colName = 'id') {
        return (int) $this->con->lastInsertId($colName);
    }

    public function insertOrUpdate($table, array $where) {
        if ($this->count($table, array('type' => $where['type'])) === 0) {
            $this->insert($table, $where);
        } else {
            $this->update($table, array('value' => $where['value']), array('type' => $where['type']));
        }
    }

}
