<?php
/*
The STATUS flag:

0 = no payment
1 = request sent
2 = Rejected
3 = confirmed
*/

class PaymentModel {

    private $db;

    public function __construct(Database $db) {
        $this->db = $db;
    }

    public function getPendingForUser($userId, $groupId) {
        $userId = App\AuthManager::checkIfUserStillAllowed($userId);

        $gid = $this->db->query(<<<SQL
            SELECT gid
            FROM mapping
            INNER JOIN users ON users.id = mapping.uid
            WHERE users.id = ?
                AND (users.status = 2 OR users.status = 3)
                AND mapping.gid = ?
            SQL, array($userId, $groupId));

        if (!$gid || $gid['gid'] == null) {
            return null; // No group
        }

        $yours = $this->db->query(<<<SQL
            SELECT user_id, total_payable, description, paid, have_to_pay, payments.status AS status
            FROM bills
            INNER JOIN payments ON payments.bill_id = bills.id
            INNER JOIN users ON users.id = payments.user_id
            INNER JOIN mapping ON mapping.uid = users.id
            WHERE user_id = ?
                AND mapping.gid = ?
                AND payments.status <> 3
            SQL, array($userId, $gid));

        $others = $this->db->query(<<<SQL
            SELECT name AS user_name, users.id AS user_id, bill_id, description, paid
            FROM payments
            INNER JOIN users ON payments.user_id = users.id
            INNER JOIN mapping ON mapping.uid = users.id
            INNER JOIN bills ON payments.bill_id = bills.id
            WHERE payments.status = 1
                AND collector = ?
                AND mapping.gid = ?
            SQL, array($userId, $gid));

        $overview = $this->db->query(<<<SQL
            SELECT name, SUM(balance) AS sum
            FROM payments
            INNER JOIN users ON users.id = payments.user_id
            INNER JOIN mapping ON mapping.uid = users.id
            WHERE mapping.gid = ?
            GROUP BY user_id
            SQL, array($gid));

        $balance = $this->db->query(<<<SQL
            SELECT SUM(balance) AS balance
            FROM payments
            INNER JOIN users ON users.id = payments.user_id
            INNER JOIN mapping ON mapping.uid = users.id
            WHERE mapping.uid = ?
                AND mapping.gid = ?
            SQL, array($userId, $gid));

        $yours = array_map(function (&$pending) {
            return array(
                'id' => (int) $pending['user_id'],
                'total' => (float) $pending['total_payable'],
                'description' => $pending['description'],
                'quantityPaid' => (float) $pending['paid'],
                'quantityOwed' => (float) $pending['have_to_pay'],
                'status' => (int) $pending['status']
            );
        }, $yours);
        $others = array_map(function (&$payment) {
            return array(
                'user' => array('name' => $payment['user_name'], 'id' => (int) $payment['user_id']),
                'amount' => (float) $payment['paid'],
                'billId' => (int) $payment['bill_id'],
                'description' => $payment['description']
            );
        }, $others);
        return array(
            'yours' => $yours,
            'others' => $others,
            'balance' => $balance,
            'overview' => $overview
        );
    }

    public function getHistoryForUser($userId, $groupId) {
        $userId = App\AuthManager::checkIfUserStillAllowed($userId);
        $isInGroup = $this->db->query(<<<SQL
            SELECT gid
            FROM mapping
            WHERE uid = ?
                AND gid = ?
            SQL, array($userId, $groupId));
        if (!$isInGroup) {
	           throw new UnexpectedValueException($GLOBALS['YouAreNotInGroup']);
        }

        return $this->db->query(<<<SQL
            SELECT
                bills.id AS billId,
                total_payable AS total,
                description,
                have_to_pay AS quantityPaid,
                payments.paid_date AS date,
                users.name AS username,
                payments.id
            FROM bills
            INNER JOIN payments ON payments.bill_id = bills.id
            INNER JOIN users ON users.id = bills.collector
            INNER JOIN mapping ON mapping.uid = users.id
            WHERE bills.group_id = ?
            GROUP BY payments.bill_id
            ORDER BY date DESC
        SQL, array($groupId));
    }

    public function deletePayment($userId, $groupId, $billId) {
        $userId = App\AuthManager::checkIfUserStillAllowed($userId);
        $isInGroup = $this->db->query(<<<SQL
            SELECT mapping.gid
            FROM mapping
            INNER JOIN users ON users.id = mapping.uid
            WHERE users.id = ?
                AND mapping.gid = ?
                AND (users.status = 2 OR users.status = 3 OR users.status = 9)
            SQL, array($userId, $groupId));

        if (!$isInGroup) {
            throw new UnexpectedValueException($GLOBALS['YouAreNotInGroup']);
        }

        $count = $this->db->count('payments', array('bill_id' => $billId, 'group_id' => $groupId));
        if (!$count) {
            throw new UnexpectedValueException($GLOBALS['InvalidBillId']);
        }
        $deletePayments = $this->db->delete('payments', array('bill_id' => $billId));
        $deleteBill = $this->db->delete('bills', array('id' => $billId));

        return $deletePayments && $deleteBill;
    }

    public function paid($userId, $sender, $receiver, $amount, $groupId) {
        $userId = App\AuthManager::checkIfUserStillAllowed($userId);
        $gid = $this->db->querySingle(<<<SQL
            SELECT gid
            FROM mapping
            INNER JOIN users ON users.id = mapping.uid
            WHERE users.id = ?
                AND (users.status = 2 OR users.status = 3)
                AND mapping.gid = ?
            SQL, array($userId, $groupId));

        $senderGroup = $this->db->querySingle(<<<SQL
            SELECT gid
            FROM mapping
            INNER JOIN users ON users.id = mapping.uid
            WHERE users.id = ?
                AND mapping.status = 2
                AND mapping.gid = ?
            SQL, array($sender, $groupId));

        $receiverGroup = $this->db->querySingle(<<<SQL
            SELECT gid
            FROM mapping
            INNER JOIN users ON users.id = mapping.uid
            WHERE users.id = ?
                AND mapping.status = 2
                AND mapping.gid = ?
            SQL, array($receiver, $groupId));

        // All users in the same group?
        if (!($gid['gid'] == $senderGroup['gid'] && $gid['gid'] == $receiverGroup['gid'])) {
            throw new UnexpectedValueException($GLOBALS['YouAreNotInGroup']);
        }
        if($amount < 0) {
            $amount = $amount * (-1);
        }
        $amount = number_format($amount, 2, '.', '');

        // Correct 'sender' balance
        $resultSender = $this->db->insert('payments', array(
                                              'user_id' => $sender,
                                              'group_id' => $senderGroup['gid'],
                                              'paid' => 0,
                                              'have_to_pay' => 0,
                                              'balance' => $amount,
                                              'paid_date' => date('Y-m-d H:i:s'),
                                              'status' => 3 ));
        // Correct 'receiver' balance
        $resultReceiver = $this->db->insert('payments', array(
                                                'user_id' => $receiver,
                                                'group_id' => $receiverGroup['gid'],
                                                'paid' => 0,
                                                'have_to_pay' => 0,
                                                'balance' => ($amount * -1),
                                                'paid_date' => date('Y-m-d H:i:s'),
                                                'status' => 3 ));

        return ($resultSender && $resultReceiver);
    }

    public function calc($details) {
        $personsCredit = array();
        $personsDebt = array();
        for($i = 0; $i < count($details['members']); $i++) {
            if($details['members'][$i]['paymentsDue'] > 0) {
                array_push($personsCredit, $details['members'][$i]);
            }
            if($details['members'][$i]['paymentsDue'] < 0) {
                array_push($personsDebt, $details['members'][$i]);
            }
        }
        if(empty($personsDebt) && empty($personsCredit)) {
            return array();
        }

        if(empty($personsDebt)) {
            return array();
        }

        $index = array(
            'debt' => 0,
            'credit' => 0,
            'paidOff' => false,
            'output' => array()
        );

        for($counter = 1; $counter <= count($personsCredit); $counter++) {
            $index = $this->fillOutputArray($index, $personsCredit, $personsDebt);
        }
        return $index['output'];
    }

    private function fillOutputArray($index, $personsCredit, $personsDebt) {
        $output = array();
        $indexDebt = $index['debt'];
        $indexCredit = $index['credit'];
        $paidOff = $index['paidOff'];
        if($personsCredit[$indexCredit]['paymentsDue'] < $personsDebt[$indexDebt]['paymentsDue'] * -1) {
            $line = array(
                'from' => $personsDebt[$indexDebt]['name'],
                'fromid' => $personsDebt[$indexDebt]['id'],
                'to' => $personsCredit[$indexCredit]['name'],
                'toid' => $personsCredit[$indexCredit]['id'],
                'value' => $personsCredit[$indexCredit]['paymentsDue']
            );
            array_push($output, $line);
            $indexCredit++;
        } else {
            while(!$paidOff && $indexDebt < count($personsDebt)) {
                $line = array(
                    'from' => $personsDebt[$indexDebt]['name'],
                    'fromid' => $personsDebt[$indexDebt]['id'],
                    'to' => $personsCredit[$indexCredit]['name'],
                    'toid' => $personsCredit[$indexCredit]['id'],
                    'value' => ($personsDebt[$indexDebt]['paymentsDue'] * -1)
                );
                array_push($output, $line);
                $indexDebt++;
                if ($personsCredit[$indexCredit]['paymentsDue'] == 0) {
                    $paidOff = true;
                }
            }
        }

        return array(
            'debt' => $indexDebt,
            'credit' => $indexCredit,
            'paidOff' => $paidOff,
            'output' => $output
        );
    }
}
