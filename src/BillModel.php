<?php

class BillModel {

    public function __construct(Database $db) {
        $this->db = $db;
    }

    private function userIdToHhId($userId, $groupId) {
        $userId = App\AuthManager::checkIfUserStillAllowed($userId);
        $gid = $this->db->querySingle(<<<SQL
            SELECT gid
            FROM mapping
            INNER JOIN users ON users.id = mapping.uid
            WHERE users.id = ?
                AND (users.status = 2 OR users.status = 3 OR users.status = 9)
                AND mapping.gid = ?
            SQL, array($userId, $groupId));

        if (!$gid || $gid == null) {
            throw new UnexpectedValueException($GLOBALS['UserDoesntBelongToThisGroup']);
        }
        return $gid['gid'];
    }

    public function addNewBill($userId, $userWhoPaid, $description, $date, $totalPayable, array $split, $groupId) {
        $userId = App\AuthManager::checkIfUserStillAllowed($userId);
        $groupId = $this->userIdToHhId($userId, $groupId);
        //////////////////////////////////////////////////////////////////////////
        $givenDate = date('Y-m-d', strtotime($date));
        $today = date('Y-m-d');
        if ($givenDate == $today) {
            $date = date('Y-m-d H:i:s');
        } else {
            $date = date('Y-m-d', strtotime($date));
        }
        //////////////////////////////////////////////////////////////////////////
        $propTotal = 0.0;
        $comp = 1.0;
        foreach ($split as $prop) {
            $propTotal += $prop;
        }
        $propTotalRounded = number_format($propTotal,4);
        $compRounded = number_format($comp, 4);
        if ($propTotalRounded !== $compRounded) {
            throw new UnexpectedValueException($GLOBALS['PercentagesDontAddUpTo100']);
        }
        if (!$this->db->insert('bills', array(
            'group_id' => $groupId,
            'total_payable' => $totalPayable,
            'description' => $description,
            'paid_date' => $date,
            'collector' => $userWhoPaid))) {
            return false;
        }
        $billId = $this->db->lastId();

        $members = $this->db->query(<<<SQL
            SELECT uid
            FROM mapping
            INNER JOIN users ON users.id = mapping.uid
            WHERE (users.status = 2 OR users.status = 3)
                AND mapping.gid = ?
            SQL, array($groupId));

        if ($members === false) {
            return false;
        }
        $membersById = array();
        foreach ($members as $member) {
            $membersById[(int) $member['uid']] = $member;
        }

        foreach ($split as $userId => $proportion) {
            if (!array_key_exists($userId, $membersById)) {
                throw new UnexpectedValueException($GLOBALS['UserIdDoesNotExistOrIsNotPartOfTheGroup']);
            }
            $array = [
                "groupId" => $groupId,
                "userId" => $userId,
                "billId" => $billId,
                "userWhoPaid" => $userWhoPaid,
                "totalPayable" => $totalPayable,
                "split" => $split,
                "proportion" => $proportion,
                "date" => $date
            ];
            $this->insertBill($array);
        }
        if(!array_key_exists($userWhoPaid, $split)) {
            $this->db->insert('payments', array(
                'user_id' => $userWhoPaid,
                'group_id' => (int) $groupId,
                'bill_id' => $billId,
                'paid' => $totalPayable,
                'have_to_pay' => 0,
                'balance' => $totalPayable,
                'paid_date' => $date,
                'status' => 3
            ));
        }
        return true;
    }

    public function sendMail($userId, $groupId, $qty) {
        $userId = App\AuthManager::checkIfUserStillAllowed($userId);
        $rounded = $this->numberFormatter($qty);

        $txt = $GLOBALS['NewBillAvailable'] . " {$rounded} " . $GLOBALS['Currency'];
        $txt .= "\r\n\r\n";

        $settlement = $this->getSettlement($userId, $groupId);

        $txt .= $GLOBALS['Balance'] . ":\r\n";

        foreach($settlement AS $member) {
            $txt .= $member['from'] . " ";
            $txt .= $GLOBALS['PayTo'] . " ";
            $txt .= $member['to'] . " ";
            $txt .= $this->numberFormatter($member['value']) . " ";
            $txt .= $GLOBALS['Currency'] . "\r\n";
        }

        Notifications::pushNotification($userId, $txt, Notifications::NEW_BILL);
    }

    public function insertBill($arr) {
        $qty = $arr['totalPayable'] * $arr['proportion'];
        $balance = $arr['totalPayable'] - $qty;

        if (count($arr['split'])) {
            if($arr['userId'] != $arr['userWhoPaid']) {
                $this->db->insert('payments', array(
                    'user_id' => $arr['userId'],
                    'group_id' => (int) $arr['groupId'],
                    'bill_id' => $arr['billId'],
                    'have_to_pay' => $qty,
                    'paid' => 0,
                    'balance' => $qty * (-1),
                    'paid_date' => $arr['date']
                ));
            } else {
                $this->db->insert('payments', array(
                    'user_id' => $arr['userWhoPaid'],
                    'group_id' => (int) $arr['groupId'],
                    'bill_id' => $arr['billId'],
                    'paid' => $arr['totalPayable'],
                    'have_to_pay' => $qty,
                    'balance' => $balance,
                    'paid_date' => $arr['date'],
                    'status' => 3
                ));
            }
        }
        $balance = 0;
        if ($arr['userId'] != $arr['userWhoPaid']) {
            $this->sendMail($arr['userId'], $arr['groupId'], $qty);
        }
    }

    public function getActiveBills($userId) {
        $userId = App\AuthManager::checkIfUserStillAllowed($userId);
        $groupId = $this->userIdToHhId($userId);

        $bills = $this->db->query(<<<SQL
            SELECT bills.id, total_payable, description, collector, users.name
            FROM bills
            JOIN users ON users.id = collector
            WHERE bills.group_id = ?
                AND paid_date IS NULL
            SQL, array($groupId));

        if ($bills === false) {
            return false;
        }
        $db = $this->db;
        return array_map(function ($bill) use (&$db, $userId) {
            $payees = $db->query(<<<SQL
                SELECT users.name, paid, have_to_pay, status
                FROM payments
                JOIN users on users.id = payments.user_id
                WHERE bill_id = ?
                ORDER BY status ASC, name ASC
                SQL, array($bill['id']));

            $payees = array_map(function ($payment) {
                return array(
                    'name' => $payment['name'],
                    'quantityPaid' => (float) $payment['paid'],
                    'quantityOwed' => (float) $payment['have_to_pay'],
                    'confirmed' => ((int) $payment['status']) === 3
                );
            }, $payees);
            return array(
                'id' => (int) $bill['id'],
                'total' => (float) $bill['total_payable'],
                'description' => $bill['description'],
                'payees' => $payees,
                'collector' => array(
                    'name' => $bill['name'],
                    'id' => (int) $bill['collector'],
                    'isCurrentUser' => (int) $bill['collector'] === $userId)
            );
        }, $bills);
    }

    public function getHistoryForUserGroup($userId) {
        $userId = App\AuthManager::checkIfUserStillAllowed($userId);
        $groupId = $this->userIdToHhId($userId);

        $bills = $this->db->query(<<<SQL
            SELECT total_payable, description, paid_date, users.name
            FROM bills
            JOIN users ON users.id = collector
            WHERE bills.group_id = ?
                AND paid_date IS NOT NULL
            SQL, array($groupId));

        if ($bills === false) {
            return false;
        }

        return array_map(function ($bill) {
            return array(
                'total' => (float) $bill['total_payable'],
                'description' => $bill['description'],
                'date' => strtotime($bill['paid_date']),
                'collectorName' => $bill['name']
            );
        }, $bills);
    }

    public function getStats($userId, $groupId) {
        $userId = App\AuthManager::checkIfUserStillAllowed($userId);
		$groupId = $this->userIdToHhId($userId, $groupId);
		$bills = array();

		for ($i = 0; $i <= 24; $i++) {
		   $month = date("Y-m-%", strtotime( date( 'Y-m-01' )." -$i months"));
		   $bill = $this->db->query('SELECT SUM(paid) AS \''.$month.'\' FROM `payments` WHERE paid_date LIKE ? AND group_id = ?', array($month, $groupId));
		   array_push($bills, $bill);
		}

		if ($bills === false) {
            return false;
        }

        $return = array();
        array_walk_recursive($bills, function($a, $b) use (&$return) {
            $key = str_replace("-%", "", $b);
            if ($a == null) {
                $a = "0.00";
            }
            $return[$key] = $a;
        });
        ksort($return);
        return $return;
	}

    public function numberFormatter($qty) {
        $precision = 2;
        $rounded = ceil($qty * pow(10, $precision)) / pow(10, $precision);
        return number_format($rounded, 2, ",", ".");
    }

    private function getSettlement($userId, $groupId) {
        $userId = App\AuthManager::checkIfUserStillAllowed($userId);
        $groupModel = new GroupModel($this->db);
        $details = $groupModel->getDetails($userId, $groupId);

        $paymentModel = new PaymentModel($this->db);
        return $paymentModel->calc($details);
    }
}
