SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- All 3 passwords are 'qwerty'

INSERT INTO `bills` (`id`, `group_id`, `total_payable`, `description`, `collector`, `paid_date`) VALUES
(1, 1, '50.00', 'Dinner', 3, '2020-01-01 00:05:00');

INSERT INTO `groups` (`id`, `name`, `owner`, `code`) VALUES
(1, 'X-Files', 3, '9261df2xxx');

INSERT INTO `mapping` (`id`, `uid`, `gid`, `is_admin`, `status`) VALUES
(1, 3, 1, 1, 2),
(2, 1, 1, 0, 2),
(3, 2, 1, 0, 2);

INSERT INTO `notifications` (`id`, `receiver_id`, `message`, `type_id`) VALUES
(1, 3, 'A new bill has been added, you owe 17 $', 2),
(2, 1, 'A new bill has been added, you owe 16.5 $', 2),
(3, 2, 'A new bill has been added, you owe 16.5 $', 2);

INSERT INTO `payments` (`id`, `user_id`, `group_id`, `bill_id`, `have_to_pay`, `paid`, `balance`, `paid_date`, `status`) VALUES
(1, 3, 1, 1, '17.00', '50.00', '33.00', '2020-01-01 00:05:00', 3),
(2, 1, 1, 1, '16.50', '0.00', '-16.50', '2020-01-01 00:05:00', 0),
(3, 2, 1, 1, '16.50', '0.00', '-16.50', '2020-01-01 00:05:00', 0),
(4, 1, 1, NULL, '0.00', '0.00', '10.00', '2020-01-01 00:05:05', 3),
(5, 3, 1, NULL, '0.00', '0.00', '-10.00', '2020-01-01 00:05:05', 3);

INSERT INTO `users` (`id`, `name`, `email`, `pass_hash`, `enabled`, `validation_code`, `registered`, `last_login`, `notifications`, `status`) VALUES
(1, 'Dana', 'scully@example.com', '$2y$12$Ki2bUmk0Q32sNbT7y9ocS.L2255P.qv4RHKlEMBu0gWWyT7MumfOK', 1, NULL, '1577833200', '1577833210', 0, 2),
(2, 'Fox', 'mulder@example.com', '$2y$12$Ki2bUmk0Q32sNbT7y9ocS.L2255P.qv4RHKlEMBu0gWWyT7MumfOK', 1, NULL, '1577833201', '1577833210', 0, 2),
(3, 'Walter', 'skinner@example.com', '$2y$12$Ki2bUmk0Q32sNbT7y9ocS.L2255P.qv4RHKlEMBu0gWWyT7MumfOK', 1, NULL, '1577833202', '1577833210', 0, 2);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
