<?php

class HomeController extends App\Controller {

    public function __construct() {
        parent::__construct('home/index');
    }

    protected function handleDefaultGet() {
        if (App\AuthManager::isLoggedIn()) {
            $template = 'group/index';
        } else {
            $template = 'auth/login';
        }
        $this->output($this->renderTemplate($template));
    }

}
