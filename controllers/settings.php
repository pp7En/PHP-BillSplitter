<?php

use App\AuthManager;

class SettingsController extends App\Controller {

    const LOCATION = 'Location: ' . ROOT;
    const SETTINGS = '/settings';
    const SETTINGS_ERROR = 'settings.error';

    public function __construct() {
        parent::__construct('settings/index');
    }

    protected function authorizeRequest($request) {
        return AuthManager::isLoggedIn();
    }

    protected function getGlobalVars() {
        $details = \UserManager::getDetails(AuthManager::getUserId());
        $error = App\SessionManager::getInstance()->get(SETTINGS_ERROR);
        App\SessionManager::getInstance()->remove(SETTINGS_ERROR);

        $build = file_get_contents('build');

        return array(
            'error' => $error,
            'user' => $details['name'],
            'email' => $details['email'],
            'build' => $build
        );
    }

    protected function handleAction($action, $args) {
        if ($args !== '') {
            return false;
        }
        $value = true;
        switch ($action) {
            case 'details':
                $this->handleDetails();
                break;
            case 'password':
                $this->handlePassword();
                break;
            case 'delete_account':
                $this->handleDeleteAccount();
                break;
            case 'set_notification':
                $this->handleSetNotification();
                break;
            case 'set_registration':
                $this->handleSetRegistration();
                break;
            case 'get_settings':
                $this->handleGetSettings();
                break;
            default:
                $value = false;
                header(self::LOCATION, 200);
                break;
        }
        return $value;
    }

    private function handleDetails() {
        $name = $this->validatePost('name', 2, 50);
        $email = $this->validatePost('email', 'email');
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $error = App\SessionManager::getInstance()->get(self::SETTINGS_ERROR, array());
            $error['message'] = $GLOBALS['InvalidEmailAddress'];
            App\SessionManager::getInstance()->put(self::SETTINGS_ERROR, $error);
        } else {
            AuthManager::changeDetails($name, $email);
        }
        $this->output($GLOBALS['DataChanged']);
    }

    private function handlePassword() {
        $oldPass = $this->validatePost('oldpass', 6);
        $passwordFirst = $this->validatePost('password-first', 6);
        $passwordSecond = $this->validatePost('password-second', 6);

        $result = AuthManager::changePassword($oldPass, $passwordFirst, $passwordSecond);
        if (is_string($result)) {
            $error = App\SessionManager::getInstance()->get(self::SETTINGS_ERROR, array());
            $error['message'] = $result;
            App\SessionManager::getInstance()->put(self::SETTINGS_ERROR, $error);
        } else {
            $data['message'] = $GLOBALS['DataChanged'];
            $this->output($GLOBALS['DataChanged']);
        }
    }

    private function handleDeleteAccount() {
        $userId = AuthManager::getUserId();
        $password = $this->validatePost('settings-delete-account-password', 6);

        $result = AuthManager::deleteAccount($userId, $password);
        if ($result !== true) {
            http_response_code(400);
            $this->output($result);
        } else {
            $this->output($GLOBALS['AccountDeleted']);
            http_response_code(200);
            sleep(2);
            $this->redirect('/', 200);
        }
    }

    private function handleGetSettings() {
        $data = array();
        $data['notification'] = AuthManager::getNotification();
        $data['registration'] = AuthManager::getRegistration(AuthManager::getUserId());

        $details = AuthManager::getDetails();

        $data['name'] = $details['name'];
        $data['email'] = $details['email'];
        $this->outputJson($data);
    }

    private function handleSetNotification() {
        $value = (isset($_POST['cbx-notification']) && $_POST['cbx-notification'] === '1') ? '1' : '0';
        AuthManager::setNotification($value);
        $this->output($GLOBALS['DataChanged']);
    }

    private function handleSetRegistration() {
        $value = (isset($_POST['cbx-registration']) && $_POST['cbx-registration'] === '1') ? '1' : '0';
        $userId = AuthManager::getUserId();
        AuthManager::setRegistration($userId, $value);
        $this->output($GLOBALS['DataChanged']);
    }
}
