<?php

use App\AuthManager;

class BillController extends App\Controller {

    const LOCATION = 'Location: ' . ROOT;

    public function __construct() {
        parent::__construct();
    }

    protected function authorizeRequest($request) {
        return AuthManager::isLoggedIn();
    }

    protected function handleAction($action, $args) {
        if ($args !== '' && $action !== 'confirm') {
            return false;
        }
        $value = true;
        switch ($action) {
            case 'setup':
                $this->handleSetup();
                break;
            case 'create':
                $this->handleCreate();
                break;
            case 'list':
                $this->handleList();
                break;
            case 'history':
                $this->handleHistory();
                break;
            case 'history.json':
                $this->handleHistoryJson();
                break;
            default:
                $value = false;
                header(self::LOCATION, 200);
                break;
        }
        return $value;
    }

    private function handleSetup() {
        $this->output($this->renderTemplate('bill/setup'));
    }

    private function handleCreate() {
        try {
            $userWhoPaid = $this->validatePost('paid-by-user-listbox', 1);
            $desc = $this->validatePost('desc', 'desc');
            $date = $this->validatePost('date', 'date');
            $total = $this->validatePost('total', 'float', 0.01);
            $splitwith = $this->validatePost('splitwith', 'array');
            $proportions = $this->validatePost('proportions', 'array');
            $groupId = $this->validatePost('groupId', 1);
            if (count($splitwith) !== count($proportions)) {
                $this->handleError(400, $GLOBALS['UserIdArrayDoesntMatchPropotionen']);
            }
            $splits = array();
            foreach ($proportions as $i => $proportion) {
                if (!ctype_digit($proportion)) {
                    $this->handleError(400, $GLOBALS['ProportionPercentageInArrayIsNotValidInteger']);
                }
                $proportion = (int) $proportion;
                if ($proportion < 1 || $proportion > 100) {
                    $this->handleError(400, $GLOBALS['ProportionMustBeBetween1and100']);
                }
                $userId = $splitwith[$i];
                if (!ctype_digit($userId)) {
                    $this->handleError(400, $GLOBALS['UserIdInArrayIsNotValidInteger']);
                }
                $splits[(int) $userId] = $proportion / 100;
            }
            $id = $this->loadModel('BillModel')->addNewBill(AuthManager::getUserId(), $userWhoPaid, $desc, $date, $total, $splits, $groupId);
            $this->checkSuccessJson($id !== false, $GLOBALS['FailedToCreateBill']);
            http_response_code(201);
        } catch (Exception $e) {
            $this->failJson($e, 500);
        }
    }

    private function handleList() {
        try {
            $bills = $this->loadModel('BillModel')->getActiveBills(AuthManager::getUserId());
        } catch (Exception $e) {
            $this->failJson($e, 400);
        }
        $this->outputJson($bills);
    }

    private function handleHistory() {
        $this->output($this->renderTemplate('bill/history'));
    }

    private function handleHistoryJson() {
        try {
            $history = $this->loadModel('BillModel')->getHistoryForUserGroup(AuthManager::getUserId());
        } catch (Exception $e) {
            $this->failJson($e, 400);
        }
        $this->outputJson($history);
    }


}
