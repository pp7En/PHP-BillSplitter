<?php

use App\AuthManager;

class StatisticsController extends App\Controller {

    const LOCATION = 'Location: ' . ROOT;

    public function __construct() {
        parent::__construct();
    }

    protected function authorizeRequest($request) {
        return AuthManager::isLoggedIn();
    }

    protected function handleAction($action, $args) {
        if ($args !== '') {
            return false;
        }
        $value = true;
        switch ($action) {
            case 'statistics.json':
                $this->getStats();
                break;
            default:
                $value = false;
                header(self::LOCATION, 200);
                break;
        }
        return $value;
    }

    private function getStats() {
        if (isset($_POST['groupId'])) {
            $groupId = $this->validatePost('groupId', 1, 10);
        } else {
            $groupId = null;
            $this->failJson($e, 400);
            return;
        }
        try {
            $history = $this->loadModel('BillModel')->getStats(AuthManager::getUserId(), $groupId);
        } catch (Exception $e) {
            $this->failJson($e, 400);
        }
        $this->outputJson($history);
    }

}
