<?php

use App\AuthManager;

class NotificationsController extends App\Controller {

    const LOCATION = 'Location: ' . ROOT;

    public function __construct() {
        parent::__construct('group/index');
    }

    protected function authorizeRequest($request) {
        return AuthManager::isLoggedIn();
    }

    protected function handleAction($action, $args) {
        if ($args !== '') {
            return false;
        }
        $value = true;
        switch ($action) {
            case 'get-notifications':
                $this->handleGetNotifications();
                break;
            case 'delete-notification':
                $this->handleDeleteNotification();
                break;
            default:
                $value = false;
                header(self::LOCATION, 200);
                break;
        }
        return $value;
    }

    private function handleGetNotifications() {
        $notifications = $this->loadModel('Notifications')->getRejectedGroupNotifications(AuthManager::getUserId());
        $this->outputJson($notifications);
    }

    private function handleDeleteNotification() {
        $id = $this->validatePost('id', 1, 10);
        $notifications = $this->loadModel('Notifications')->deleteNotification($id, AuthManager::getUserId());
        $this->outputJson($notifications);
    }

}
