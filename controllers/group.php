<?php

use App\AuthManager;

class GroupController extends App\Controller {

    const LOCATION = 'Location: ' . ROOT;
    const GROUP = '/group';

    public function __construct() {
        parent::__construct('group/index');
    }

    protected function authorizeRequest($request) {
        return AuthManager::isLoggedIn();
    }

    protected function handleAction($action, $args) {
        if ($args !== '') {
            return false;
        }
        $value = true;
        switch ($action) {
            case 'details':
                $this->handleDetails();
                break;
            case 'create':
                $this->handleCreate();
                break;
            case 'join':
                $this->handleJoin();
                break;
            case 'members':
                $this->handleListMembers();
                break;
            case 'transmit':
                $this->handleTransmitGroup();
                break;
            case 'get-requests':
                $this->handleGetRequests();
                break;
            case 'confirm-request':
                $this->handleConfirmRequest();
                break;
            case 'reject-request':
                $this->handleRejectRequest();
                break;
            case 'create-new-user':
                $this->handleCreateNewUser();
                break;
            case 'remove-user':
                $this->handleRemoveUser();
                break;
            case 'get-groups':
                $this->handleGetGroups();
                break;
            case 'settlement':
                $this->handleSettlement();
                break;
            case 'change-guest-login':
                $this->handleChangeGuestLogin();
                break;
            default:
                $value = false;
                header(self::LOCATION, 200);
                break;
        }
        return $value;
    }

    private function handleDetails() {
        $groupId = $this->validatePost('groupId', 1, 10);
        $details = $this->loadModel('GroupModel')->getDetails(AuthManager::getUserId(), $groupId);
        $this->outputJson($details);
    }

    private function handleCreate() {
        $name = $this->validatePost('name', 2, 50);
        $userId = intval(AuthManager::getUserId());
        $id = $this->loadModel('GroupModel')->create($name, $userId);
        $this->checkSuccessJson($id !== false, $GLOBALS['FailedToCreateGroup']);
        header(self::LOCATION, 200);
        exit();
    }

    private function handleJoin() {
        $groupCode = $this->validatePost('code', 1, 10);
        try {
            $success = $this->loadModel('GroupModel')->requestJoin(AuthManager::getUserId(), $groupCode);
        } catch (Exception $e) {
            $this->failSimpleMessage($e);
        }
        $this->checkSuccessJson($success, $GLOBALS['FailedToRequestJoiningThatGroup']);
        http_response_code(204);
    }

    private function handleListMembers() {
        if (isset($_POST['groupId'])) {
            $groupId = $this->validatePost('groupId', 1, 10);
        } else {
            $groupId = null;
        }
        $members = $this->loadModel('GroupModel')->getGroupMembers(AuthManager::getUserId(), $groupId);
        if ($members === null) {
            $this->failJson($GLOBALS['ThatUserIsNotRegisteredToGroup'], 400);
            return false;
        }
        array_push($members, AuthManager::getUserId());
        $this->outputJson($members);
    }

    private function handleGetRequests() {
        $session = App\SessionManager::getInstance();
        if ($session->get('guest')) {
            $this->outputJson('');
        }
        $groupId = $this->validatePost('groupId', 1, 10);
        try {
            $details = $this->loadModel('GroupModel')->getRequestJoins(AuthManager::getUserId(), $groupId);
            if(is_array($details)) {
                $this->outputJson($details);
            } else {
                return false;
            }
        } catch (\Exception $e) {
            $this->failSimpleMessage($e);
        }

    }

    private function handleConfirmRequest() {
        $newUserId = $this->validatePost('newUserId', 1, 10);
        $groupId = $this->validatePost('groupId', 1, 10);
        try {
            $details = $this->loadModel('GroupModel')->getConfirmJoins(AuthManager::getUserId(), $newUserId, $groupId);
            $this->outputJson($details);
            header(self::LOCATION, 200);
        } catch (\Exception $e) {
            $this->failSimpleMessage($e);
        }
    }

    private function handleRejectRequest() {
        $newUserId = $this->validatePost('newUserId', 1, 10);
        $groupId = $this->validatePost('groupId', 1, 10);
        try {
            $details = $this->loadModel('GroupModel')->rejectRequest(AuthManager::getUserId(), $newUserId, $groupId);
            $this->outputJson($details);
            header(self::LOCATION, 200);
        } catch (\Exception $e) {
            $this->failSimpleMessage($e);
        }
    }

    private function handleTransmitGroup() {
        if (isset($_POST['new-admin-listbox'])) {
            $newUserId = $this->validatePost('new-admin-listbox', 1, 10);
        } else {
            $newUserId = false;
        }
        $groupId = $this->validatePost('groupId', 1, 10);
        try {
            $this->loadModel('GroupModel')->transmitGroup(AuthManager::getUserId(), $newUserId, $groupId);
            header(self::LOCATION, 200);
        } catch (\Exception $e) {
            $this->failSimpleMessage($e);
        }
    }

    private function handleCreateNewUser() {
        $guest = $this->validatePost('guestname', 1, 50);
        $groupId = $this->validatePost('groupId', 1, 10);
        try {
            $detail = $this->loadModel('GroupModel')->createNewGuestUser(AuthManager::getUserId(), $guest, $groupId);
            $this->outputJson($detail);
        } catch (\Exception $e) {
            $this->failSimpleMessage($e);
        }
    }

    private function handleRemoveUser() {
        $guest = $this->validatePost('userId', 1, 10);
        $groupId = $this->validatePost('groupId', 1, 10);
        try {
            $this->loadModel('GroupModel')->removeUser(AuthManager::getUserId(), $guest, $groupId);
            header(self::LOCATION, 200);
        } catch (\Exception $e) {
            $this->failSimpleMessage($e);
        }
    }

    private function handleGetGroups() {
        $details = $this->loadModel('GroupModel')->getAllGroups(AuthManager::getUserId());
        if (count($details) == 0) {
            header("HTTP/1.1 500 " . $GLOBALS['ThatUserIsNotRegisteredToGroup']);
            return false;
        } else {
            $this->outputJson($details);
        }
    }

    private function handleSettlement() {
        $groupId = $this->validatePost('groupId', 1, 10);
        $details = $this->loadModel('GroupModel')->getDetails(AuthManager::getUserId(), $groupId);
        $settlement = $this->loadModel('PaymentModel')->calc($details);
        $this->outputJson($settlement);
    }

    private function handleChangeGuestLogin() {
        $groupId = $this->validatePost('groupId', 1, 10);
        $value = $this->validatePost('cbx-change-guest-login');
        if ($value != 'fetch' && isset($_POST['cbx-change-guest-login'])) {
            error_log($_POST['cbx-change-guest-login']);
            $value = $_POST['cbx-change-guest-login'];
        }
        $details = $this->loadModel('GroupModel')->changeGuestLoginForGroup(AuthManager::getUserId(), $groupId, $value);
        $this->outputJson($details);
    }
}
