<?php

use App\AuthManager;

class AuthController extends App\Controller {

    const LOCATION = 'Location: ' . ROOT;
    const AUTH_REGISTER = '/auth/register';
    const AUTH_LOGIN = '/auth/login';
    const LOGIN_EMAIL = 'login.email';
    const LOGIN_ERROR = 'login.error';
    const REG_EMAIL = 'reg.email';
    const REG_NAME = 'reg.name';
    const REG_ERROR = 'reg.error';


    public function __construct() {
        parent::__construct();
    }

    protected function handleAction($action, $args) {
        if ($args !== '') {
            return;
        }
        if (isset($_GET['activation'])) {
            $this->handleActivation();
            return;
        }
        if (isset($_GET['code'])) {
            $this->output($this->renderTemplate('auth/reset-password'));
            return;
        }

        switch ($action) {
            case 'login':
                $this->handleLogin();
                break;
            case 'register':
                $this->handleRegister();
                break;
            case 'logout':
                $this->handleLogout();
                break;
            case 'activation':
                $this->handleSuccessActivation();
                break;
            case 'restore-password':
                $this->handlePasswordReset();
                break;
            case 'apply-new-password':
                $this->handleApplyNewPassword();
                break;
            case 'guest-login':
                $this->handleGuestLogin();
                break;
            default:
                header(self::LOCATION, 200);
                break;
        }
    }

    private function handleLogin() {
        if (AuthManager::isLoggedIn()) {
            $this->redirect('/');
        }
        $session = App\SessionManager::getInstance();

        if ($this->isGet()) {
            $form = array('email' => $session->get(self::LOGIN_EMAIL));
            $this->output($this->renderTemplate('auth/login', array('messages' => $session->get(self::LOGIN_ERROR), 'form' => $form)));
            $session->remove(self::LOGIN_EMAIL);
            $session->remove(self::LOGIN_ERROR);
            return;
        }

        $email = $this->validatePost('email', 'email');
        $hashed_password = $this->validatePost('password', 'password');

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $error = $session->get(self::LOGIN_ERROR, array());
            $error['email'] = $GLOBALS['InvalidEmailAddress'];
            $session->put(self::LOGIN_ERROR, $error);
            $session->put(self::LOGIN_EMAIL, $email);
            $this->redirect(self::AUTH_LOGIN, 303);
        }

        $resp = AuthManager::login($email, $hashed_password);
        $session->put(self::LOGIN_EMAIL, $email);

        if ($resp['success']) {
            $this->redirect('/', 303);
        } else {
            error_log("Login failed", 0);
            $session->put(self::LOGIN_ERROR, $resp['messages']);
            $this->redirect(self::AUTH_LOGIN, 303);
        }
    }

    private function handleRegister() {
        if (AuthManager::isLoggedIn()) {
            $this->redirect('/');
        }
        if (AuthManager::getRegistrationValue() == 0) {
            echo $this->renderTemplate('error/error', array('code' => '404', 'message' => $GLOBALS['RegistrationDisabled']));
            return;
        }
        $session = App\SessionManager::getInstance();

        if ($this->isGet()) {
            $form = array('email' => $session->get(self::REG_EMAIL), 'name' => $session->get(self::REG_NAME));
            $message = $session->get(self::REG_ERROR);
            if($message == NULL) {
                $message = "";
            }
            if ($form['email'] == NULL) {
                $form['email'] = "";
            }
            if ($form['name'] == NULL) {
                $form['name'] = "";
            }
            $this->output($this->renderTemplate('auth/register', array('messages' => $message, 'form' => $form)));
            $session->remove(self::REG_EMAIL);
            $session->remove(self::REG_NAME);
            $session->remove(self::REG_ERROR);
            return;
        }

        $name = $this->validatePost('name', 2);
        $email = $this->validatePost('email', 'email');
        $passwordFirst = $this->validatePost('password-first', 6);
        $passwordSecond = $this->validatePost('password-second', 6);
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $error = $session->get(self::REG_ERROR, array());
            $error['email'] = $GLOBALS['InvalidEmailAddress'];
            $session->put(self::REG_ERROR, $error);
            $session->put(self::REG_EMAIL, $email);
            $session->put(self::REG_NAME, $name);
            $this->redirect(self::AUTH_REGISTER, 303);
        }

        $resp = AuthManager::register($name, $email, $passwordFirst, $passwordSecond);
        if ($resp['success']) {
            $error['sended'] = $GLOBALS['ActivationMailSended'];
            $session->put(self::REG_ERROR, $error);
            $session->put(self::REG_EMAIL, $email);
            $session->put(self::REG_NAME, $name);
            $this->redirect(self::AUTH_REGISTER, 303);
        }
        $session->put(self::REG_EMAIL, $email);
        $session->put(self::REG_NAME, $name);
        $session->put(self::REG_ERROR, $resp['messages']);
        $this->redirect(self::AUTH_REGISTER, 303);
    }

    private function handleLogout() {
        AuthManager::logout();
        $this->redirect('/');
    }

    private function handleActivation() {
        $code = $this->validateGet('activation');
        AuthManager::validateAccount($code);
        $this->redirect('/auth/activation', 303);
    }

    private function handleSuccessActivation() {
        $this->output($this->renderTemplate('auth/activation'));
    }

    private function handlePasswordReset() {
        $session = App\SessionManager::getInstance();
        $email = $this->validatePost('restore-password-email', 'email');
        $resp = AuthManager::passwordReset($email);
        if ($resp['success']) {
            $error['sended'] = $GLOBALS['ResetPasswordMailSended'];
            $session->put(self::REG_ERROR, $error);
            $session->put(self::REG_EMAIL, $email);
            $this->redirect(self::AUTH_LOGIN, 303);
        }
        $session->put(self::REG_EMAIL, $email);
        $session->put(self::REG_ERROR, $resp['messages']);
        $this->redirect(self::AUTH_LOGIN, 303);
    }

    private function handleApplyNewPassword() {
        $session = App\SessionManager::getInstance();
        $validationCode = $this->validatePost('validation-code', 'validation');
        $passwordFirst = $this->validatePost('apply-new-password-first', 6);
        $passwordSecond = $this->validatePost('apply-new-password-second', 6);

        $resp = AuthManager::appylNewPassword($validationCode, $passwordFirst, $passwordSecond);
        if ($resp['success']) {
            $error['sended'] = $GLOBALS['AccountResetPasswordSuccess'];
            $session->put(self::REG_ERROR, $error);
            $session->put('reg.passwordFirst', $passwordFirst);
            $session->put('reg.passwordSecond', $passwordSecond);
            $this->redirect(self::AUTH_LOGIN, 303);
        }
        $session->put('reg.passwordFirst', $passwordFirst);
        $session->put('reg.passwordSecond', $passwordSecond);
        $session->put(self::REG_ERROR, $resp['messages']);
        $this->redirect(self::AUTH_LOGIN, 303);
    }

    private function handleGuestLogin(){
        $session = App\SessionManager::getInstance();

        $gid = $this->validatePost('gid', 'int');
        $hashedCode = $this->validatePost('autologincode', 20);
        $resp = AuthManager::autoGuestLogin($gid, $hashedCode);

        if ($resp['success']) {
            $session->put('guest', true);
            $this->redirect('/', 303);
        } else {
            error_log("Login failed", 0);
            $session->put(self::LOGIN_ERROR, $resp['messages']);
            $this->redirect(self::AUTH_LOGIN, 303);
        }
    }
}
