<?php

use App\AuthManager;

class PaymentController extends App\Controller {

    const LOCATION = 'Location: ' . ROOT;

    public function __construct() {
        parent::__construct();
    }

    protected function authorizeRequest($request) {
        return AuthManager::isLoggedIn();
    }

    protected function handleAction($action, $args) {
        if ($args !== '' && $action !== 'make' && $action !== 'cancel') {
            return false;
        }
        $value = true;
        switch ($action) {
            case 'history':
                $this->handleHistoryHtml();
                break;
            case 'history.json':
                $this->handleHistoryJson();
                break;
            case 'pending':
                $this->handlePending();
                break;
            case 'delete-payment':
                $this->handleDeletePayment();
                break;
            case 'paid':
                $this->handlePaid();
                break;
            default:
                $value = false;
                header(self::LOCATION, 200);
                break;
        }
        return $value;
    }

    private function handleHistoryHtml() {
        $this->output($this->renderTemplate('payment/history'));
    }

    private function handleHistoryJson() {
        if (isset($_POST['groupId'])) {
            $groupId = $this->validatePost('groupId', 1, 10);
        } else {
            $groupId = null;
        }
        $history = $this->loadModel('PaymentModel')->getHistoryForUser(AuthManager::getUserId(), $groupId);
        $this->outputJson($history);
    }

    private function handlePending() {
        $pending = $this->loadModel('PaymentModel')->getPendingForUser(AuthManager::getUserId());
        $this->outputJson($pending);
    }

    private function handleDeletePayment() {
        $billId = $this->validatePost('billId', 1, 10);
        $groupId = $this->validatePost('groupId', 1, 10);
        $success = $this->loadModel('PaymentModel')->deletePayment(AuthManager::getUserId(), $groupId, $billId);
        $this->checkSuccessJson($success, $GLOBALS['FailedToDeletePayment']);
        http_response_code(204);
    }

    private function handlePaid() {
        error_log('here');
        $sender = $this->validatePost('from', 1, 10);
        $receiver = $this->validatePost('to', 1, 10);
        $modeOfPayment = $this->validatePost('modeOfPayment', 'mode-of-payment');
        if ($modeOfPayment == 'partly') {
            $amount = $this->validatePost('amountOfPartlyPayment', 1, 10);
        } else {
            $amount = $this->validatePost('amount', 1, 10);
        }
        $groupId = $this->validatePost('groupId', 1, 10);
        $success = $this->loadModel('PaymentModel')->paid(AuthManager::getUserId(), $sender, $receiver, $amount, $groupId);
        $this->output($success);
        http_response_code(204);
    }

}
