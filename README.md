# BillSplitter

I forked this application from simon816 (https://github.com/simon816/BillSplitter). This application has not been tested for security and should not be accessible to the public without further review. But I think you can use it for public. For me, this application has been running without errors in the local network for a long time.

Please note that I can't give support for this application due to time restrictions. Nevertheless I would like to give a short overview where you can find what.

## Webserver

If you use an apache2, note the `.htaccess` file. If you are using a nginx, then note the `nginx.example.conf`.

## Database

You can use either SQLite or a database server. You can set the properties and credentials in `config.exmaple.php`. Please rename the file to `config.php` after that. The database schema can be found in `db_scheme.sql`.

## Languages

This application supports English and German. The language files can be found in `/public/i18n/*`. I haven't tested the English version for a long time, but it should work. There is one file for PHP and one file for the JavaScript part. Which language should be used, you can define in the `config.exmaple.php`.

## HTML Templates

The templates can be found in `/view/*.html`. All HTML files are minified with the first call and are then named `/view/*.min.html`.

## CSS and JavaScript

All files that need to be public and directly accessible can be found in `/public/*`. There are also CSS and JavaScript files. Both will be minified at the first call and will be named `*.min.css` or `*.min.js`.

## Donations

[<img src="https://codeberg.org/pp7En/PHP-BillSplitter/raw/branch/main/buymeacoffee.png" alt="Buy Me A Coffee" width="214"/>](https://www.buymeacoffee.com/warpcoredev)
